/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import app.softwork.serialization.csv.CSVFormat
import ch.memobase.models.EnrichedFacetContainer
import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import ch.memobase.models.config.ApplicationConfig
import ch.memobase.models.config.DrupalApiClientConfiguration
import ch.memobase.models.config.ElasticsearchClientConfig
import ch.memobase.models.config.IndexConfiguration
import ch.memobase.models.data.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.builtins.ListSerializer
import org.apache.logging.log4j.LogManager
import java.io.File


@OptIn(ExperimentalSerializationApi::class)
class Configuration {

    val drupal = DrupalApiClientConfiguration(
        load("DRUPAL_API_DOMAIN", "test.memobase.ch"),
        load("DRUPAL_API_KEY", null, true),
        load("DRUPAL_API_USERNAME", null, true),
        load("DRUPAL_API_PASSWORD", null, true),
    )

    val elasticSearchConfig: ElasticsearchClientConfig = ElasticsearchClientConfig(
        load("ES_HOSTS", "https://localhost:8080"),
        load("ES_API_KEY_ID", null, true),
        load("ES_API_KEY_SECRET", null, true),
        load("ES_CA_CERT_PATH", "certs/ca.crt"),
        load("ES_USE_CUSTOM_CA_CERT", "false").toBoolean(),
    )

    val indexConfig = IndexConfiguration(
        load("ENVIRONMENT", "test"),
        load("ES_REPORTING_INDEX", "memobase-reports-drupal-ingest-service-test-v1"),
        load("ES_DOCUMENTS_INDEX", "memobase-documents-search-test-v1"),
        load("ES_RECORD_SETS_INDEX", "memobase-record-sets-search-test-v1"),
        load("ES_RECORD_SETS_INDEX_MAPPING_FILE_PATH", "mappings/record-sets-mapping.json"),
        load("ES_RECORD_SETS_INDEX_SETTING_FILE_PATH", "settings/record-sets-settings.json"),
        load("ES_INSTITUTIONS_INDEX", "memobase-institutions-search-test-v1"),
        load("ES_INSTITUTIONS_INDEX_MAPPING_FILE_PATH", "mappings/institutions-mapping.json"),
        load("ES_INSTITUTIONS_INDEX_SETTING_FILE_PATH", "settings/institutions-settings.json"),
        load("ES_RECORD_SETS_API_INDEX", "memobase-record-sets-api-test-v1"),
        load("ES_RECORD_SETS_API_INDEX_MAPPING_FILE_PATH", "mappings/record-sets-api-mapping.json"),
        load("ES_RECORD_SETS_API_INDEX_SETTING_FILE_PATH", "settings/record-sets-api-settings.json"),
        load("ES_INSTITUTIONS_API_INDEX", "memobase-institutions-api-test-v1"),
        load("ES_INSTITUTIONS_API_INDEX_MAPPING_FILE_PATH", "mappings/institutions-api-mapping.json"),
        load("ES_INSTITUTIONS_API_INDEX_SETTING_FILE_PATH", "settings/institutions-api-settings.json")
    )

    val application = ApplicationConfig(
        load("APPLICATION_HOST", "0.0.0.0"),
        load("APPLICATION_PORT", "5000").toInt(),
    )

    private val cantonsCsvPath = load("APPLICATION_CANTON_CSV_PATH", "data/cantons.csv")
    val cantons = csv.decodeFromString(ListSerializer(Canton.serializer()), loadFile(cantonsCsvPath)).associate {
        Pair(
            it.code, FacetContainer(
                name = LanguageContainer(
                    it.de, it.fr, it.it
                ),
                filter = it.code,
                facet = listOf(it.id)
            )
        )
    }

    private val municipalityCsvPath = load("APPLICATION_MUNICIPALITY_CSV_PATH", "data/municipalities.csv")
    val municipalities = run {
        val result = mutableMapOf<String, List<Municipality>>()
        csv.decodeFromString(ListSerializer(Municipality.serializer()), loadFile(municipalityCsvPath)).map {
            Pair(
                it.postalCode, it
            )
        }.map { (postalCode, facet) ->
            result[postalCode] = result.getOrDefault(postalCode, emptyList()) + facet
        }
        result
    }
    val inventoryDocumentTypes = loadInventoryDocumentTypes()

    private val inventoryGeoPath = load("APPLICATION_INVENTORY_GEO_CSV_PATH", "data/inventoryGeo.csv")
    val inventoryGeo =
        csv.decodeFromString(ListSerializer(TaxonomyLabels.serializer()), loadFile(inventoryGeoPath)).associate {
            Pair(
                it.drupalUuid, EnrichedFacetContainer(
                    name = LanguageContainer(
                        it.de, it.fr, it.it
                    ),
                    displayLabel = LanguageContainer(
                        it.de.removeRange(0, 2), it.fr.removeRange(0, 2), it.it.removeRange(0, 2)
                    ),
                    type = it.drupalUuid
                )
            )
        }
    private val inventoryTimePeriodPath =
        load("APPLICATION_INVENTORY_TIME_PERIOD_CSV_PATH", "data/inventoryTimePeriod.csv")
    val inventoryTimePeriod =
        csv.decodeFromString(ListSerializer(TaxonomyLabels.serializer()), loadFile(inventoryTimePeriodPath)).associate {
            Pair(
                it.drupalUuid, EnrichedFacetContainer(
                    name = LanguageContainer(
                        it.de, it.fr, it.it
                    ),
                    displayLabel = LanguageContainer(
                        it.de.removeRange(0, 2), it.fr.removeRange(0, 2), it.it.removeRange(0, 2)
                    ),
                    type = it.drupalUuid
                )
            )
        }


    companion object {
        private val log = LogManager.getLogger(this::class.java)

        private val csv = CSVFormat()
        fun loadInventoryDocumentTypes(): Map<InventoryDocumentTypeValue, FacetContainer> {
            val inventoryDocumentTypesPath =
                load("APPLICATION_INVENTORY_DOCUMENT_TYPES_CSV_PATH", "data/inventoryDocumentTypes.csv")
            return csv.decodeFromString(
                ListSerializer(InventoryDocumentType.serializer()), loadFile(inventoryDocumentTypesPath)
            ).associate {
                Pair(
                    it.id, FacetContainer(
                        name = LanguageContainer(
                            it.de, it.fr, it.it
                        ),
                        filter = it.id.name.lowercase()
                    )
                )
            }
        }

        fun load(env: String, default: String?, secure: Boolean = false): String {
            val value = System.getenv(env)
            if (value == null) {
                if (default != null) {
                    log.warn("Environment variable $env not set, using default value $default.")
                    return default
                }
                log.error("Environment variable $env not set.")
                throw IllegalStateException("Environment variable $env not set.")
            } else {
                if (secure) {
                    log.info("Using environment variable $env.")
                } else {
                    log.info("Using environment variable $env with value $value.")
                }
                return value
            }
        }

        fun loadFile(path: String): String {
            return File(path).readText(Charsets.UTF_8)
        }
    }
}
