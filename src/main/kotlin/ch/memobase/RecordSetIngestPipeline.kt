/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.impl.DrupalApiClient
import ch.memobase.impl.ElasticClient
import ch.memobase.impl.RecordSetDataCollectionPipeline
import ch.memobase.impl.RecordSetDataCollectionPipeline.Companion.RELATED_RECORD_SET_NO_LINK_VALUE
import ch.memobase.models.LanguageContainer
import ch.memobase.models.config.IndexConfiguration
import ch.memobase.models.language
import ch.memobase.models.recordset.*
import ch.memobase.models.responses.ExceptionReport
import ch.memobase.rdf.NS
import ch.memobase.utility.Date
import ch.memobase.utility.DrupalApiException
import ch.memobase.utility.ElasticsearchException
import ch.memobase.utility.InvalidDataException
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage


class RecordSetIngestPipeline(
    private val drupalAPIClient: DrupalApiClient,
    private val elasticClient: ElasticClient,
    private val indexConfig: IndexConfiguration,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val dataCollectionPipeline =
        RecordSetDataCollectionPipeline(drupalAPIClient, elasticClient, indexConfig.documentsIndex)

    init {
        with(elasticClient) {
            checkIndex(
                indexConfig.recordSetsIndex,
                indexConfig.recordSetsIndexMappingFilePath,
                indexConfig.recordSetsApiIndexSettingFilePath
            )
            checkIndex(
                indexConfig.recordSetsApiIndex,
                indexConfig.recordSetsApiIndexMappingFilePath,
                indexConfig.recordSetsApiIndexSettingFilePath
            )
        }
    }

    suspend fun ingestAll() {
        val recordSetUuids = drupalAPIClient.getAllRecordSetUuids()
        recordSetUuids.forEach { uuid ->
            ingest(uuid, "none")
        }
        log.info("Ingested all ${recordSetUuids.size} record sets.")
    }

    suspend fun ingest(uuid: String, memobaseId: String) {
        val data = try {
            dataCollectionPipeline.collectRecordSetData(uuid)
        } catch (ex: ElasticsearchException) {
            errorReporting(uuid, memobaseId, ex, false)
            return
        } catch (ex: DrupalApiException) {
            errorReporting(uuid, memobaseId, ex, false)
            return
        } catch (ex: Exception) {
            errorReporting(uuid, memobaseId, ex, true)
            return
        }
        val id = data.dataDe.attributes.fieldMemobaseId
        val warning = if (data.documentTypes.isEmpty()) {
            "There are no documents in the index for this record set."
        } else {
            ""
        }
        try {
            val recordSetSearchDocument = buildRecordSetDocument(uuid, data)
            val recordSetApiDocument = buildRecordSetApiDocument(data)
            elasticClient.indexDocument(
                indexConfig.recordSetsIndex, data.dataDe.attributes.fieldMemobaseId, recordSetSearchDocument.getReader()
            )
            elasticClient.indexDocument(
                indexConfig.recordSetsApiIndex, data.dataDe.attributes.fieldMemobaseId, recordSetApiDocument.getReader()
            )


            val updateNecessary = elasticClient.createPipeline(
                recordSetId = recordSetSearchDocument.id,
                environment = indexConfig.environment,
                recordSetName = recordSetSearchDocument.name,
                institutions = recordSetSearchDocument.institution,
                originalInstitutions = recordSetSearchDocument.originalInstitution,
                masterInstitutions = recordSetSearchDocument.masterInstitution,
                accessInstitutions = recordSetSearchDocument.accessInstitution
            )


            if (warning.isNotEmpty()) {
                elasticClient.indexDocument(
                    indexConfig.reportingIndex, uuid, ExceptionReport(
                        uuid,
                        id,
                        "Warning",
                        warning,
                        type = "RecordSet",
                    ).getReader()
                )
            } else {
                elasticClient.indexDocument(
                    indexConfig.reportingIndex, uuid, ExceptionReport(
                        uuid,
                        id,
                        "Success",
                        "Record Set successfully ingested. Counted ${data.numberOfDocuments} documents total and ${data.numberOfDocumentsPublished} published documents.\n\n${updateNecessary.second}",
                        type = "RecordSet",
                    ).getReader()
                )
            }

            if (updateNecessary.first) {
                elasticClient.updateByQuery(
                    index = indexConfig.documentsIndex,
                    recordSetId = recordSetSearchDocument.id,
                    environment = indexConfig.environment,
                    recordSetName = recordSetSearchDocument.name,
                    institutions = recordSetSearchDocument.institution,
                    originalInstitutions = recordSetSearchDocument.originalInstitution,
                    masterInstitutions = recordSetSearchDocument.masterInstitution,
                    accessInstitutions = recordSetSearchDocument.accessInstitution
                )
            }
        } catch (ex: ElasticsearchException) {
            errorReporting(uuid, id, ex, false)
        } catch (ex: DrupalApiException) {
            errorReporting(uuid, id, ex, false)
        } catch (ex: InvalidDataException) {
            errorReporting(uuid, id, ex, false)
        } catch (ex: Exception) {
            errorReporting(uuid, id, ex, true)
        }
    }

    private fun errorReporting(uuid: String, id: String?, ex: Exception, stacktrace: Boolean) {
        elasticClient.indexDocument(
            indexConfig.reportingIndex, uuid, ExceptionReport(
                uuid,
                id,
                ex::class.java.name,
                ex.localizedMessage,
                type = "RecordSet",
                stackTrace = if (stacktrace) ex.stackTraceToString() else null
            ).getReader()
        )
        log.error("Failed to import Record Set $id ($uuid): $ex.")
    }

    private fun buildRecordSetDocument(
        uuid: String, data: RecordSetCompleteData,
    ): RecordSetDocument {
        val periodOfTime = data.dataDe.attributes.fieldTimePeriod?.let { value ->
            value.split("/").takeIf { it.size == 2 }?.let { (from, to) ->
                IntegerRange(
                    from.toIntOrNull() ?: 0, to.toIntOrNull() ?: 0
                )
            }
        }

        return recordSetDocument {
            id = data.dataDe.attributes.fieldMemobaseId
            published = data.dataDe.attributes.status
            name = LanguageContainer(
                data.dataDe.attributes.title, data.dataFr.attributes.title, data.dataIt.attributes.title
            )
            drupalUuid = uuid
            keyVisualLink = data.dataDe.attributes.computedTeaserImageUrl
            numberOfDocuments = data.numberOfDocuments
            numberOfDocumentsPublished = data.numberOfDocumentsPublished
            teaserText = LanguageContainer(
                data.dataDe.attributes.fieldProcessedTeaserText?.value,
                data.dataFr.attributes.fieldProcessedTeaserText?.value,
                data.dataIt.attributes.fieldProcessedTeaserText?.value
            )
            documentType = data.documentTypes
            periodOfTimeAsYear = periodOfTime
            periodOfTimeAsYearTo = periodOfTime?.gte.toString()
            periodOfTimeAsYearFrom = periodOfTime?.lte.toString()
            supportedByMemoriav = data.dataDe.attributes.fieldSupportedByMemoriav
            languageOfMetadata = data.metadataCodes.map {
                language(it)
            }
            institution = data.institutions
            scopeAndContent = LanguageContainer(
                data.dataDe.attributes.fieldScope?.value,
                data.dataFr.attributes.fieldScope?.value,
                data.dataIt.attributes.fieldScope?.value
            )
            accessMemobase = LanguageContainer(
                data.dataDe.attributes.fieldAccessMemobase?.value,
                data.dataFr.attributes.fieldAccessMemobase?.value,
                data.dataIt.attributes.fieldAccessMemobase?.value
            )
            context = LanguageContainer(
                data.dataDe.attributes.fieldContext?.value,
                data.dataFr.attributes.fieldContext?.value,
                data.dataIt.attributes.fieldContext?.value
            )
            originalTitle = LanguageContainer(
                data.dataDe.attributes.fieldOriginalTitle,
                data.dataFr.attributes.fieldOriginalTitle,
                data.dataIt.attributes.fieldOriginalTitle
            )
            extent = LanguageContainer(
                data.dataDe.attributes.fieldContent?.value,
                data.dataFr.attributes.fieldContent?.value,
                data.dataIt.attributes.fieldContent?.value
            )
            selection = LanguageContainer(
                data.dataDe.attributes.fieldSelection?.value,
                data.dataFr.attributes.fieldSelection?.value,
                data.dataIt.attributes.fieldSelection?.value
            )
            indexing = LanguageContainer(
                data.dataDe.attributes.fieldInfoOnDevelopment?.value,
                data.dataFr.attributes.fieldInfoOnDevelopment?.value,
                data.dataIt.attributes.fieldInfoOnDevelopment?.value
            )
            rights = LanguageContainer(
                data.dataDe.attributes.fieldRights?.value,
                data.dataFr.attributes.fieldRights?.value,
                data.dataIt.attributes.fieldRights?.value
            )
            description = LanguageContainer(
                data.dataDe.attributes.fieldDataTransfer?.value,
                data.dataFr.attributes.fieldDataTransfer?.value,
                data.dataIt.attributes.fieldDataTransfer?.value
            )
            access = LanguageContainer(
                data.dataDe.attributes.fieldAccess?.value,
                data.dataFr.attributes.fieldAccess?.value,
                data.dataIt.attributes.fieldAccess?.value
            )
            project = LanguageContainer(
                data.dataDe.attributes.fieldProject.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataFr.attributes.fieldProject.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataIt.attributes.fieldProject.mapNotNull { it.title }.filter { it.isNotEmpty() },
            )
            relatedRecordSets = if (data.relatedRecordSets.isNotEmpty()) data.relatedRecordSets.map {
                it.name
            }.reduce { acc, languageContainer ->
                acc.merge(languageContainer)
            } else null
            relatedPublications = LanguageContainer(
                data.dataDe.attributes.fieldPublications.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataFr.attributes.fieldPublications.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataIt.attributes.fieldPublications.mapNotNull { it.title }.filter { it.isNotEmpty() },
            )
            relatedDocuments = LanguageContainer(
                data.dataDe.attributes.fieldDocuments.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataFr.attributes.fieldDocuments.mapNotNull { it.title }.filter { it.isNotEmpty() },
                data.dataIt.attributes.fieldDocuments.mapNotNull { it.title }.filter { it.isNotEmpty() },
            )
            dataImport = LanguageContainer(
                data.dataDe.attributes.fieldTransferDate,
                data.dataFr.attributes.fieldTransferDate,
                data.dataIt.attributes.fieldTransferDate
            )
            originalCallNumber = data.dataDe.attributes.fieldOriginalShelfMark
            originalIdNumber = data.dataDe.attributes.fieldOriginalId

            originalInstitution = data.originalInstitutions
            masterInstitution = data.masterInstitutions
            accessInstitution = data.accessInstitutions
        }
    }

    private fun buildRecordSetApiDocument(
        data: RecordSetCompleteData,
    ): RecordSetJsonLd {
        val id = data.dataDe.attributes.fieldMemobaseId
        val namespacedRecordSetId = "${NS.mbrs}$id"
        val ricoRelatedRecordSets = data.relatedRecordSets.map {
            RicoRecordResource(
                rdfType = RdfType.RECORD_SET,
                sameAs = it.filter,
                titleDe = it.name.de.firstOrNull(),
                titleFr = it.name.fr.firstOrNull(),
                titleIt = it.name.it.firstOrNull(),
                isRecordResourceAssociatedWithRecordResource = namespacedRecordSetId,
            )
        }

        val relatedDocumentsDe = data.dataDe.attributes.fieldDocuments
        val relatedDocumentsFr = data.dataFr.attributes.fieldDocuments
        val relatedDocumentsIt = data.dataIt.attributes.fieldDocuments
        val relatedDocuments = mutableListOf<RicoRecordResource>()

        relatedDocumentsDe.map {
            RicoRecordResource(
                rdfType = RdfType.RECORD,
                sameAs = if (!it.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)) it.uri else null,
                titleDe = it.title,
                isRecordResourceAssociatedWithRecordResource = namespacedRecordSetId,
            )
        }.forEach { relatedDocuments.add(it) }

        relatedDocumentsFr.map {
            RicoRecordResource(
                rdfType = RdfType.RECORD,
                sameAs = if (!it.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)) it.uri else null,
                titleFr = it.title,
                isRecordResourceAssociatedWithRecordResource = namespacedRecordSetId,
            )
        }.forEach { relatedDocuments.add(it) }

        relatedDocumentsIt.map {
            RicoRecordResource(
                rdfType = RdfType.RECORD,
                sameAs = if (!it.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)) it.uri else null,
                titleIt = it.title,
                isRecordResourceAssociatedWithRecordResource = namespacedRecordSetId,
            )
        }.forEach { relatedDocuments.add(it) }

        mergeRelated(RdfType.RECORD, relatedDocuments, namespacedRecordSetId)

        return RecordSetJsonLd(
            id = namespacedRecordSetId,
            conditionsOfAccessDe = data.dataDe.attributes.fieldAccess?.value,
            conditionsOfAccessFr = data.dataFr.attributes.fieldAccess?.value,
            conditionsOfAccessIt = data.dataIt.attributes.fieldAccess?.value,
            conditionsOfUseDe = data.dataDe.attributes.fieldRights?.value,
            conditionsOfUseFr = data.dataFr.attributes.fieldRights?.value,
            conditionsOfUseIt = data.dataIt.attributes.fieldRights?.value,
            conformsToDe = data.dataDe.attributes.fieldInfoOnDevelopment?.value,
            conformsToFr = data.dataFr.attributes.fieldInfoOnDevelopment?.value,
            conformsToIt = data.dataIt.attributes.fieldInfoOnDevelopment?.value,
            descriptiveNoteDe = data.dataDe.attributes.fieldDataTransfer?.value,
            descriptiveNoteFr = data.dataFr.attributes.fieldDataTransfer?.value,
            descriptiveNoteIt = data.dataIt.attributes.fieldDataTransfer?.value,
            hasLanguageOfResourceDe = data.dataDe.attributes.fieldLanguage,
            hasLanguageOfResourceFr = data.dataFr.attributes.fieldLanguage,
            hasLanguageOfResourceIt = data.dataIt.attributes.fieldLanguage,
            hasOrHadHolder = data.institutions.mapNotNull { it.filter },
            hasOrHadIdentifier = ricoIdentifiers(id, namespacedRecordSetId, data),
            hasOrHadLanguage = ricoLanguages(data, namespacedRecordSetId),
            hasOrHadTitle = ricoTitles(data, namespacedRecordSetId),
            hasRestrictionOnAccessDe = data.dataDe.attributes.fieldAccessMemobase?.value,
            hasRestrictionOnAccessFr = data.dataFr.attributes.fieldAccessMemobase?.value,
            hasRestrictionOnAccessIt = data.dataIt.attributes.fieldAccessMemobase?.value,
            hasSource = ricoSource(data, namespacedRecordSetId),
            hasSponsoringAgentOfResource = sponsoringAgents(data, namespacedRecordSetId),
            historyDe = data.dataDe.attributes.fieldContext?.value,
            historyFr = data.dataFr.attributes.fieldContext?.value,
            historyIt = data.dataIt.attributes.fieldContext?.value,
            image = data.dataDe.attributes.computedTeaserImageUrl,
            integrityDe = data.dataDe.attributes.fieldSelection?.value,
            integrityFr = data.dataFr.attributes.fieldSelection?.value,
            integrityIt = data.dataIt.attributes.fieldSelection?.value,
            isAssociatedWithDate = data.dataDe.attributes.fieldTimePeriod?.let {
                RicoDate(
                    normalizedDateValue = it,
                    isDateAssociatedWith = namespacedRecordSetId,
                )
            },
            isOrWasSubjectOf = mapPublications(data, namespacedRecordSetId),
            isPublished = data.dataDe.attributes.status,
            isRecordResourceAssociatedWithRecordResource = ricoRelatedRecordSets + relatedDocuments,
            modificationDate = Date.now(),
            publicationDate = data.dataDe.attributes.fieldTransferDate?.split("T")?.get(0),
            recordResourceExtentDe = data.dataDe.attributes.fieldScope?.value,
            recordResourceExtentFr = data.dataFr.attributes.fieldScope?.value,
            recordResourceExtentIt = data.dataIt.attributes.fieldScope?.value,
            recordResourceOrInstantiationIsTargetOfRecordResourceHoldingRelation = holdingRelations(
                data, namespacedRecordSetId
            ),
            scopeAndContentDe = data.dataDe.attributes.fieldContent?.value,
            scopeAndContentFr = data.dataFr.attributes.fieldContent?.value,
            scopeAndContentIt = data.dataIt.attributes.fieldContent?.value,
            titleDe = data.dataDe.attributes.title,
            titleFr = data.dataFr.attributes.title,
            titleIt = data.dataIt.attributes.title
        )
    }

    private fun mapPublications(
        data: RecordSetCompleteData, namespacedRecordSetId: String,
    ): List<RicoSubjectRecord> {
        val ricoSubjectRecords = mutableMapOf<String, RicoSubjectRecord>()

        for ((languageData, languageCode) in listOf(data.dataDe to "de", data.dataFr to "fr", data.dataIt to "it")) {
            val publications = languageData.attributes.fieldPublications

            publications.forEach { publication ->
                if (publication.uri.isBlank()) {
                    throw IllegalArgumentException("Publication uri of publication ${publication.title} is blank.")
                }
                val existingRecord = ricoSubjectRecords[publication.uri]
                ricoSubjectRecords[publication.uri] = RicoSubjectRecord(
                    titleDe = if (languageCode == "de") publication.title else existingRecord?.titleDe,
                    titleFr = if (languageCode == "fr") publication.title else existingRecord?.titleFr,
                    titleIt = if (languageCode == "it") publication.title else existingRecord?.titleIt,
                    sameAs = if (!publication.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)) publication.uri else null,
                    hasOrHadSubject = namespacedRecordSetId
                )
            }
        }

        return ricoSubjectRecords.values.toList()
    }


    private fun ricoIdentifiers(
        id: String, namespacedRecordSetId: String, data: RecordSetCompleteData,
    ): MutableList<RicoIdentifier> {
        val identifiers = mutableListOf<RicoIdentifier>()
        identifiers.add(
            RicoIdentifier(
                identifier = id, type = IdentifierType.MAIN, isOrWasIdentifierOf = namespacedRecordSetId
            )
        )
        data.dataDe.attributes.fieldOriginalShelfMark?.let {
            identifiers.add(
                RicoIdentifier(
                    identifier = it, type = IdentifierType.CALL_NUMBER, isOrWasIdentifierOf = namespacedRecordSetId
                )
            )
        }
        data.dataDe.attributes.fieldOriginalId?.let {
            identifiers.add(
                RicoIdentifier(
                    identifier = it, type = IdentifierType.ORIGINAL, isOrWasIdentifierOf = namespacedRecordSetId
                )
            )
        }
        data.dataDe.attributes.fieldOldMemobaseId?.let {
            identifiers.add(
                RicoIdentifier(
                    identifier = it, type = IdentifierType.OLD_MEMOBASE, isOrWasIdentifierOf = namespacedRecordSetId
                )
            )
        }
        return identifiers
    }

    private fun ricoLanguages(
        data: RecordSetCompleteData, namespacedRecordSetId: String,
    ): MutableList<RicoLanguage> {
        val metadataLanguages = mutableListOf<RicoLanguage>()
        data.metadataCodes.forEach {
            when (it) {
                "de" -> metadataLanguages.add(
                    RicoLanguage.german(namespacedRecordSetId)
                )

                "fr" -> metadataLanguages.add(
                    RicoLanguage.french(namespacedRecordSetId)
                )

                "it" -> metadataLanguages.add(
                    RicoLanguage.italian(namespacedRecordSetId)
                )
            }
        }
        return metadataLanguages
    }

    private fun sponsoringAgents(data: RecordSetCompleteData, namespacedRecordSetId: String): List<SponsoringAgent> {
        val sponsoringAgents = mutableListOf<SponsoringAgent>()
        val mappedProjects = mutableMapOf<String, SponsoringAgent>()

        fun addProject(titleDe: String?, titleFr: String?, titleIt: String?, uri: String) {
            if (uri.isBlank() || uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)) {
                sponsoringAgents.add(
                    SponsoringAgent.memoriavProject(
                        titleDe = titleDe, titleFr = titleFr, titleIt = titleIt, sameAs = null,
                        recordSet = namespacedRecordSetId
                    )
                )
            } else {
                val project = mappedProjects[uri]
                if (project != null) {
                    mappedProjects[uri] = project.copy(
                        titleDe = titleDe ?: project.titleDe,
                        titleFr = titleFr ?: project.titleFr,
                        titleIt = titleIt ?: project.titleIt,
                        sameAs = project.sameAs.let {
                            (it ?: emptyList()) + uri
                        }.toSet().toList()
                    )
                } else {
                    mappedProjects[uri] = SponsoringAgent.memoriavProject(
                        titleDe = titleDe, titleFr = titleFr, titleIt = titleIt,
                        sameAs = listOf(uri), recordSet = namespacedRecordSetId
                    )
                }
            }

        }

        data.dataDe.attributes.fieldProject.forEach {
            addProject(it.title, null, null, it.uri)
        }

        data.dataFr.attributes.fieldProject.forEach {
            addProject(null, it.title, null, it.uri)
        }

        data.dataIt.attributes.fieldProject.forEach {
            addProject(null, null, it.title, it.uri)
        }

        if (data.dataDe.attributes.fieldSupportedByMemoriav) {
            sponsoringAgents.add(SponsoringAgent.memoriav())
        }

        sponsoringAgents.addAll(mappedProjects.values)
        return sponsoringAgents
    }


    private fun ricoSource(
        data: RecordSetCompleteData, namespacedRecordSetId: String,
    ): RicoSource? {
        if (data.dataDe.attributes.fieldOriginalDescription == null && data.dataFr.attributes.fieldOriginalDescription == null && data.dataIt.attributes.fieldOriginalDescription == null) {
            return null
        }
        val sameAs = mutableSetOf<String>()
        if (data.dataDe.attributes.fieldOriginalDescription?.uri != null &&
            data.dataDe.attributes.fieldOriginalDescription.uri.isNotBlank() &&
            !data.dataDe.attributes.fieldOriginalDescription.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)
        ) {
            sameAs.add(data.dataDe.attributes.fieldOriginalDescription.uri)
        }

        if (data.dataFr.attributes.fieldOriginalDescription?.uri != null &&
            data.dataFr.attributes.fieldOriginalDescription.uri.isNotBlank() &&
            !data.dataFr.attributes.fieldOriginalDescription.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)
        ) {
            sameAs.add(data.dataFr.attributes.fieldOriginalDescription.uri)
        }

        if (data.dataIt.attributes.fieldOriginalDescription?.uri != null &&
            data.dataIt.attributes.fieldOriginalDescription.uri.isNotBlank() &&
            !data.dataIt.attributes.fieldOriginalDescription.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE)
        ) {
            sameAs.add(data.dataIt.attributes.fieldOriginalDescription.uri)
        }

        return RicoSource(
            titleDe = data.dataDe.attributes.fieldOriginalDescription?.title,
            titleFr = data.dataFr.attributes.fieldOriginalDescription?.title,
            titleIt = data.dataIt.attributes.fieldOriginalDescription?.title,
            sameAs = sameAs.toList(),
            isSourceOf = namespacedRecordSetId,
        )
    }

    private fun ricoTitles(
        data: RecordSetCompleteData, namespacedRecordSetId: String,
    ): MutableList<RicoTitle> {
        val titles = mutableListOf<RicoTitle>()
        titles.add(
            RicoTitle(
                type = TitleType.MAIN,
                titleDe = data.dataDe.attributes.title,
                titleFr = data.dataFr.attributes.title,
                titleIt = data.dataIt.attributes.title,
                isOrWasTitleOf = namespacedRecordSetId
            )
        )
        if (data.dataDe.attributes.fieldOriginalTitle != null || data.dataFr.attributes.fieldOriginalTitle != null || data.dataIt.attributes.fieldOriginalTitle != null) {
            titles.add(
                RicoTitle(
                    type = TitleType.ORIGINAL,
                    titleDe = data.dataDe.attributes.fieldOriginalTitle,
                    titleFr = data.dataFr.attributes.fieldOriginalTitle,
                    titleIt = data.dataIt.attributes.fieldOriginalTitle,
                    isOrWasTitleOf = namespacedRecordSetId
                )
            )
        }
        return titles
    }

    private fun holdingRelations(
        data: RecordSetCompleteData, namespacedRecordSetId: String,
    ): List<RicoRecordResourceHoldingRelation> {
        val holdingRelations = mutableListOf<RicoRecordResourceHoldingRelation>()
        data.originalInstitutions.forEach { facetContainer ->
            holdingRelations.add(RicoRecordResourceHoldingRelation(
                type = RecordResourceHoldingRelationType.ORIGINAL,
                recordResourceHoldingRelationHasTarget = namespacedRecordSetId,
                recordResourceHoldingRelationHasSource = facetContainer.filter?.let { identifier ->
                    "${NS.mbcb}$identifier"
                } ?: throw InvalidDataException(
                    "Original institution identifier of record set with id ${data.dataDe.attributes.fieldMemobaseId} not present."
                ),
            ))
        }
        data.masterInstitutions.forEach { facetContainer ->
            holdingRelations.add(RicoRecordResourceHoldingRelation(
                type = RecordResourceHoldingRelationType.MASTER,
                recordResourceHoldingRelationHasTarget = namespacedRecordSetId,
                recordResourceHoldingRelationHasSource = facetContainer.filter?.let { identifier ->
                    "${NS.mbcb}$identifier"
                } ?: throw InvalidDataException(
                    "Master institution identifier of record set with id ${data.dataDe.attributes.fieldMemobaseId} not present."
                ),
            ))
        }
        data.accessInstitutions.forEach { facetContainer ->
            holdingRelations.add(RicoRecordResourceHoldingRelation(
                type = RecordResourceHoldingRelationType.ACCESS,
                recordResourceHoldingRelationHasTarget = namespacedRecordSetId,
                recordResourceHoldingRelationHasSource = facetContainer.filter?.let { identifier ->
                    "${NS.mbcb}$identifier"
                } ?: throw InvalidDataException(
                    "Access institution identifier of record set with id ${data.dataDe.attributes.fieldMemobaseId} not present."
                ),
            ))
        }
        return holdingRelations
    }


    private fun mergeRelated(
        rdfType: RdfType, relatedRecordSets: List<RicoRecordResource>, namespacedRecordSetId: String,
    ): List<RicoRecordResource> {

        val mergedList = mutableListOf<RicoRecordResource>()
        val mergedMap = mutableMapOf<String, MutableList<String>>()

        for (recordSet in relatedRecordSets) {
            if (recordSet.sameAs == null) {
                mergedList.add(recordSet)
                continue
            }
            val titles = mergedMap.getOrPut(recordSet.sameAs) { mutableListOf() }
            recordSet.titleDe?.let { titles.add(it) }
            recordSet.titleFr?.let { titles.add(it) }
            recordSet.titleIt?.let { titles.add(it) }
        }

        for ((sameAs, titles) in mergedMap) {
            val titleDe = titles.firstOrNull { it.isNotBlank() }
            val titleFr = titles.firstOrNull { it.isNotBlank() && it != titleDe }
            val titleIt = titles.firstOrNull { it.isNotBlank() && it != titleDe && it != titleFr }

            mergedList.add(
                RicoRecordResource(
                    rdfType = rdfType,
                    sameAs = sameAs,
                    titleDe = titleDe,
                    titleFr = titleFr,
                    titleIt = titleIt,
                    isRecordResourceAssociatedWithRecordResource = namespacedRecordSetId
                )
            )
        }

        return mergedList
    }
}


