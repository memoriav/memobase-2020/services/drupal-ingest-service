/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.plugins.configureHTTP
import ch.memobase.plugins.configureRouting
import ch.memobase.plugins.configureSerialization
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

val configuration = Configuration()

fun main() {
    val log = LogManager.getLogger("main")
    val server = embeddedServer(
        Netty,
        port = configuration.application.port,
        host = configuration.application.host,
        module = Application::module
    )

    try {
        server.start(wait = false)
        log.info("Server started at http://${configuration.application.host}:${configuration.application.port}/openapi")
    } catch (e: IllegalStateException) {
        log.error("Failed to start server because of a configuration issue", e)
        exitProcess(1)
    } catch (e: Exception) {
        log.error("Failed to start server", e)
        exitProcess(1)
    }

    Runtime.getRuntime().addShutdownHook(Thread {
        server.stop(1000, 1000)
        exitProcess(0)
    })
}

fun Application.module() {
    configureHTTP()
    configureSerialization()
    configureRouting()
}


