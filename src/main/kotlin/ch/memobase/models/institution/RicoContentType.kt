/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.institution

import ch.memobase.Configuration
import ch.memobase.models.data.InventoryDocumentTypeValue
import ch.memobase.models.recordset.RdfType
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoContentType @OptIn(ExperimentalSerializationApi::class) constructor(
    @EncodeDefault
    @SerialName("@type")
    val rdfType: RdfType = RdfType.CONTENT_TYPE,
    val isOrWasContentTypeOfAllMembersOf: String,
    val nameDe: String,
    val nameFr: String,
    val nameIt: String,
) {
    companion object {
        private val documentTypes = Configuration.loadInventoryDocumentTypes()
        private val names =
            InventoryDocumentTypeValue.entries.toTypedArray().associateWith {
                if (it in documentTypes) documentTypes[it]!!.name else null
            }
        fun fromType(inventoryDocumentTypeValue: InventoryDocumentTypeValue, collection: String): RicoContentType {
            val name = names[inventoryDocumentTypeValue] ?: error("No name found for $inventoryDocumentTypeValue")
            return RicoContentType(
                isOrWasContentTypeOfAllMembersOf = collection,
                nameDe = name.de.first(),
                nameFr = name.fr.first(),
                nameIt = name.it.first(),
            )
        }

    }
}
