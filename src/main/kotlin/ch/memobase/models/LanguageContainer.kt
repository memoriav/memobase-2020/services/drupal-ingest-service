/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models

import ch.memobase.models.data.LanguageCode
import kotlinx.serialization.Serializable

@Serializable
data class LanguageContainer(
    val de: List<String>,
    val fr: List<String>,
    val it: List<String>,
) {
    constructor() : this(emptyList(), emptyList(), emptyList())

    constructor(de: String?, fr: String?, it: String?) : this(
        de?.let { if (it.isNotEmpty()) listOf(it) else emptyList() } ?: emptyList(),
        fr?.let { if (it.isNotEmpty()) listOf(it) else emptyList() } ?: emptyList(),
        it?.let { if (it.isNotEmpty()) listOf(it) else emptyList() } ?: emptyList(),
    )

    fun merge(other: LanguageContainer): LanguageContainer {
        return LanguageContainer(
            de + other.de,
            fr + other.fr,
            it + other.it,
        )
    }

    fun addValue(language: LanguageCode, value: String): LanguageContainer {
        if (value.isEmpty()) return this
        return when (language) {
            LanguageCode.DE -> copy(de = de + value)
            LanguageCode.FR -> copy(fr = fr + value)
            LanguageCode.IT -> copy(it = it + value)
        }
    }
}