plugins {
    application
    distribution
    jacoco
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.jvm
    id("org.jetbrains.kotlin.jvm") version "2.0.20"
    // https://plugins.gradle.org/plugin/io.ktor.plugin
    id("io.ktor.plugin") version "2.3.12"
    // https://plugins.gradle.org/plugin/io.freefair.git-version
    id("io.freefair.git-version") version "8.10"
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.plugin.serialization
    id("org.jetbrains.kotlin.plugin.serialization") version "2.0.20"
    // id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    // https://plugins.gradle.org/plugin/org.jetbrains.dokka
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

application {
    mainClass.set("ch.memobase.ApplicationKt")
    tasks.withType<Tar>().configureEach {
        archiveFileName = "app.tar"
    }
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
    }
}

dependencies {
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities/-/tags
    implementation("ch.memobase:memobase-service-utilities:4.15.3")

    // https://central.sonatype.com/artifact/io.ktor/ktor
    implementation("io.ktor:ktor-server-core-jvm")
    implementation("io.ktor:ktor-server-openapi")
    implementation("io.ktor:ktor-server-content-negotiation-jvm")
    implementation("io.ktor:ktor-server-netty-jvm")
    implementation("io.ktor:ktor-client-core-jvm")
    implementation("io.ktor:ktor-client-content-negotiation-jvm")
    implementation("io.ktor:ktor-client-cio-jvm")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm")
    // https://central.sonatype.com/artifact/com.charleskorn.kaml/kaml?smo=true
    implementation("com.charleskorn.kaml:kaml:0.61.0")
    // https://central.sonatype.com/artifact/org.elasticsearch/elasticsearch
    implementation("co.elastic.clients:elasticsearch-java:8.15.3")
    // https://central.sonatype.com/artifact/com.fasterxml.jackson.module/jackson-module-kotlin
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.18.+")

    // https://mvnrepository.com/artifact/io.swagger.parser.v3/swagger-parser-v3
    implementation("io.swagger.parser.v3:swagger-parser-v3:2.1.22")

    // https://central.sonatype.com/artifact/app.softwork/kotlinx-serialization-csv
    implementation("app.softwork:kotlinx-serialization-csv:0.0.18")
    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
    implementation("org.apache.logging.log4j:log4j-api:2.24.1")
    implementation("org.apache.logging.log4j:log4j-core:2.24.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-slf4j18-impl
    implementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.18.0")
    testImplementation("io.ktor:ktor-server-tests-jvm")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:2.0.20")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.3")
    // https://central.sonatype.com/artifact/io.mockk/mockk
    testImplementation("io.mockk:mockk:1.13.13")
}


configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}