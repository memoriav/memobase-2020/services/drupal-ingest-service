/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import ch.memobase.models.config.ElasticsearchClientConfig
import ch.memobase.models.documents.DocumentSchema
import ch.memobase.utility.ElasticsearchException
import ch.memobase.utility.InvalidHostException
import ch.memobase.utility.hashedWithSha256
import co.elastic.clients.elasticsearch.ElasticsearchClient
import co.elastic.clients.elasticsearch._types.FieldValue
import co.elastic.clients.elasticsearch._types.HealthStatus
import co.elastic.clients.elasticsearch._types.Result
import co.elastic.clients.elasticsearch._types.Time
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery
import co.elastic.clients.elasticsearch._types.query_dsl.Query
import co.elastic.clients.elasticsearch._types.query_dsl.TermQuery
import co.elastic.clients.elasticsearch.core.*
import co.elastic.clients.elasticsearch.core.search.PointInTimeReference
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest
import co.elastic.clients.elasticsearch.indices.ExistsRequest
import co.elastic.clients.elasticsearch.ingest.GetPipelineRequest
import co.elastic.clients.elasticsearch.ingest.Processor
import co.elastic.clients.json.JsonData
import co.elastic.clients.json.JsonpMappingException
import co.elastic.clients.json.jackson.JacksonJsonpMapper
import co.elastic.clients.transport.ElasticsearchTransport
import co.elastic.clients.transport.rest_client.RestClientTransport
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.apache.http.HttpHost
import org.apache.http.message.BasicHeader
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.ssl.SSLContexts
import org.apache.logging.log4j.LogManager
import org.elasticsearch.client.RestClient
import java.io.File
import java.io.IOException
import java.io.Reader
import java.net.SocketTimeoutException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.util.*
import javax.net.ssl.SSLContext
import kotlin.system.exitProcess


class ElasticClient(private val cfg: ElasticsearchClientConfig) {
    private val encodedApiKey =
        Base64.getEncoder().encodeToString(("${cfg.apiKeyId}:${cfg.apiKeySecret}").toByteArray(StandardCharsets.UTF_8))
    private val log = LogManager.getLogger(this::class.java)
    private val client: ElasticsearchClient
    private val hostRegex = Regex("^(https?)://(.*):(\\d+)$")

    companion object {
        private const val PIPELINE_NAME_PREFIX = "memobase-update-"
        private const val DOCUMENT_TYPE_SEARCH_SORT_FIELD = "id"
        private const val DOCUMENT_TYPE_SEARCH_SIZE = 1000
        private const val WAIT_FOR_COMPLETION_SLEEP_DURATION_MS = 1000L
        private const val DOCUMENT_FIELD_RECORD_SET = "recordSet"
        private const val DOCUMENT_FIELD_INSTITUTION = "institution"
        private const val DOCUMENT_FIELD_ORIGINAL_INSTITUTION = "originalInstitution"
        private const val DOCUMENT_FIELD_MASTER_INSTITUTION = "masterInstitution"
        private const val DOCUMENT_FIELD_ACCESS_INSTITUTION = "accessInstitution"
        private const val DOCUMENT_TERM_QUERY_FIELD_RECORD_SET = "recordSet.facet"
        private const val DOCUMENT_SCHEMA_DOCUMENT_TYPE_FIELD_NAME = "type"
        private const val DOCUMENT_TERM_QUERY_FIELD_PUBLISHED = "published"
    }

    init {
        client = createClient()
        checkHealth()
    }

    private fun createClient(): ElasticsearchClient {
        val parsedHosts = try {
            parseHost(cfg.hosts)
        } catch (ex: InvalidHostException) {
            log.error("Invalid host: ${ex.message}")
            throw ElasticsearchException("Invalid host: ${ex.message}")
        }
        val restClientBuilder = RestClient.builder(*parsedHosts)
            .setDefaultHeaders(arrayOf(BasicHeader("Authorization", "ApiKey $encodedApiKey")))
        if (cfg.useCustomCaCert) {
            restClientBuilder
                .setHttpClientConfigCallback { httpClientBuilder ->
                    httpClientBuilder.setSSLContext(buildSSLContext(cfg.certificateAuthorityPath))
                }
        }
        // this enables Kotlin data classes to be used by Jackson!
        val mapper = JacksonJsonpMapper(jacksonObjectMapper())
        val transport: ElasticsearchTransport = RestClientTransport(restClientBuilder.build(), mapper)
        return ElasticsearchClient(transport)
    }

    private fun checkHealth() {
        try {
            val response = client.cluster().health {
                it.waitForStatus(HealthStatus.Green)
            }
            log.info("Elasticsearch cluster health: ${response.status()}")
        } catch (ex: Exception) {
            log.error("Elasticsearch cluster health failed: ${ex.localizedMessage}")
            throw ElasticsearchException("Elasticsearch cluster health failed: ${ex.localizedMessage}")
        }
    }

    fun checkIndex(
        indexName: String, indexMappingFilePath: String, indexSettingFilePath: String,
    ) {
        val indexExistsResponse = client.indices().exists(
            ExistsRequest.Builder().index(indexName).build()
        )
        if (indexExistsResponse.value()) {
            log.info("Index $indexName exists.")
        } else {
            log.info("Index $indexName does not exist. Creating it now!")
            createIndex(indexName, indexMappingFilePath, indexSettingFilePath)
        }
    }

    private fun createIndex(
        indexName: String, indexMappingFilePath: String, indexSettingFilePath: String,
    ) {
        val response =
            client.indices().create(CreateIndexRequest.Builder().index(indexName).mappings { mappingBuilder ->
                try {
                    val indexMapping = File(indexMappingFilePath).inputStream()
                    mappingBuilder.withJson(indexMapping)
                } catch (ex: IOException) {
                    log.error("Error loading index mapping: ${ex.localizedMessage}")
                    throw ElasticsearchException("Error loading index mapping: ${ex.localizedMessage}")
                } catch (ex: JsonpMappingException) {
                    log.error("Error parsing index mapping: ${ex.localizedMessage}")
                    throw ElasticsearchException("Error parsing index mapping: ${ex.localizedMessage}")
                }
                mappingBuilder
            }.settings { settingsBuilder ->
                try {
                    val indexSettings = File(indexSettingFilePath).inputStream()
                    settingsBuilder.withJson(indexSettings)
                } catch (ex: IOException) {
                    log.error("Error loading index settings: ${ex.localizedMessage}")
                    throw ElasticsearchException("Error loading index settings: ${ex.localizedMessage}")
                } catch (ex: JsonpMappingException) {
                    log.error("Error parsing index settings: ${ex.localizedMessage}")
                    throw ElasticsearchException("Error parsing index settings: ${ex.localizedMessage}")
                }
                settingsBuilder
            }.build())
        if (response.acknowledged()) {
            log.info("Index $indexName created.")
        } else {
            log.error("Index $indexName creation failed.")
            throw ElasticsearchException("Index $indexName creation failed.")
        }
    }


    fun indexDocument(indexName: String, id: String, document: Reader, create: Boolean = false) {
        val maxRetries = 5
        var attempt = 0
        var waitTime = 1000L
        while (attempt < maxRetries) {
            try {
                val response = if (create) {
                    val request = CreateRequest.of { it: CreateRequest.Builder<String> ->
                        it.index(indexName).id(id).withJson(document)
                    }
                    client.create(request)
                } else {
                    val request = IndexRequest.of { it: IndexRequest.Builder<String> ->
                        it.index(indexName).id(id).withJson(document)
                    }
                    client.index(request)
                }
                when (response.result()) {
                    Result.Created -> {
                        log.info("Document $id created in index $indexName.")
                        return
                    }
                    Result.Updated -> {
                        log.info("Document $id updated in index $indexName.")
                        return
                    }
                    Result.Deleted -> throw ElasticsearchException("Deleted a document instead...")
                    Result.NotFound -> throw ElasticsearchException("Document $id indexing failed: ${response.result()}")
                    Result.NoOp -> throw ElasticsearchException("Document $id indexing failed: ${response.result()}")
                    else -> throw ElasticsearchException("Document $id indexing failed: ${response.result()}")
                }
            } catch (ex: SocketTimeoutException) {
                attempt += 1
                log.warn("Counting documents failed due to timeout. Retrying attempt $attempt/$maxRetries in $waitTime ms...")
                Thread.sleep(waitTime)
                waitTime *= 2
            } catch (ex: Exception) {
                throw ElasticsearchException("Document $id indexing failed for unknown reason: ${ex.localizedMessage}")
            }
        }
    }



    fun countTotalNumberOfDocuments(recordSetIdentifier: String, index: String): Long {
        return countNumberOfDocuments(
            index, Query.Builder().bool(
                BoolQuery.Builder().must(
                    Query.Builder()
                        .term(
                            TermQuery.Builder().field(DOCUMENT_TERM_QUERY_FIELD_RECORD_SET).value(recordSetIdentifier)
                                .build()
                        )
                        .build()
                ).build()
            ).build()
        )
    }

    fun countNumberOfDocumentsUnpublished(recordSetIdentifier: String, index: String): Long {
        return countNumberOfDocuments(
            index, Query.Builder().bool(
                BoolQuery.Builder().must(
                    Query.Builder()
                        .term(
                            TermQuery.Builder().field(DOCUMENT_TERM_QUERY_FIELD_RECORD_SET).value(recordSetIdentifier)
                                .build()
                        )
                        .build()
                ).must(
                    Query.Builder()
                        .term(TermQuery.Builder().field(DOCUMENT_TERM_QUERY_FIELD_PUBLISHED).value(false).build())
                        .build()

                ).build()
            ).build()
        )
    }

    private fun countNumberOfDocuments(index: String, query: Query): Long {
        val maxRetries = 5
        var attempt = 0
        var waitTime = 1000L

        while (attempt < maxRetries) {
            try {
                val countResponse: CountResponse? = client.count(
                    CountRequest.Builder().query(query).index(index).build()
                )
                return countResponse?.count() ?: throw ElasticsearchException("Count response is null.")
            } catch (ex: Exception) {
                if (ex is SocketTimeoutException) {
                    log.warn("Counting documents failed due to timeout. Retrying attempt $attempt/$maxRetries in $waitTime ms...")
                    Thread.sleep(waitTime)
                    waitTime *= 2
                } else {
                    log.error("Counting documents failed: ${ex.localizedMessage}")
                    throw ElasticsearchException("Counting documents failed: ${ex.localizedMessage}")
                }
            }
            attempt += 1
        }

        throw ElasticsearchException("Counting documents failed after $maxRetries attempts.")
    }

    fun getDocumentTypesFromRecords(
        recordSetIdentifier: String, indexName: String,
    ): List<FacetContainer> {
        val result = mutableMapOf<String, FacetContainer>()
        val pointInTime = try {
            val pointInTimeResponse = client.openPointInTime(OpenPointInTimeRequest.of { builder ->
                builder.index(indexName)
                    .keepAlive(Time.of {
                        it.time("10m")
                    })
            })

            PointInTimeReference.of {
                it.id(pointInTimeResponse.id())
            }
        } catch (ex: Exception) {
            log.error("Point in time creation failed: ${ex.localizedMessage}")
            throw ElasticsearchException("Point in time creation failed: ${ex.localizedMessage}")
        }

        try {
            var response = client.search(documentTypeSearch(recordSetIdentifier, pointInTime, null), DocumentSchema::class.java)
            while (response.hits().hits().isNotEmpty()) {
                for (hit in response.hits().hits()) {
                    val doc = hit.source()
                    if (doc != null) {
                        val filter = doc.type.filter
                        if (filter != null && !result.containsKey(filter)) {
                            result[filter] = doc.type
                        }
                    }
                }
                val searchAfter = response.hits().hits().last().sort()
                response = client.search(
                    documentTypeSearch(recordSetIdentifier, pointInTime, searchAfter), DocumentSchema::class.java
                )
            }
        } catch (ex: Exception) {
            log.error("Document type search failed: ${ex.localizedMessage}")
            throw ElasticsearchException("Document type search failed: ${ex.localizedMessage}")
        }
        return result.values.toList()
    }

    private fun documentTypeSearch(
        recordSetIdentifier: String,
        pointInTime: PointInTimeReference,
        searchAfter: List<FieldValue>?,
    ): SearchRequest {
        val searchQuery =
            Query.Builder().term(
                TermQuery.Builder().field(DOCUMENT_TERM_QUERY_FIELD_RECORD_SET).value(recordSetIdentifier).build()
            ).build()
        return SearchRequest.of { builder ->
            if (searchAfter != null) {
                builder.searchAfter(searchAfter)
            }

            builder
                .query(searchQuery)
                .size(DOCUMENT_TYPE_SEARCH_SIZE)
                .sort { sortBuilder ->
                    sortBuilder
                        .field {
                            it.field(DOCUMENT_TYPE_SEARCH_SORT_FIELD)
                        }
                }
                .source { sourceBuilder ->
                    sourceBuilder
                        .filter { filterBuilder ->
                            filterBuilder.includes(DOCUMENT_SCHEMA_DOCUMENT_TYPE_FIELD_NAME)
                        }
                }
                .pit(PointInTimeReference.of { it.id(pointInTime.id()) })
        }
    }

    fun createPipeline(
        recordSetId: String,
        environment: String,
        recordSetName: LanguageContainer,
        institutions: List<FacetContainer>,
        accessInstitutions: List<FacetContainer>,
        masterInstitutions: List<FacetContainer>,
        originalInstitutions: List<FacetContainer>,
    ): Pair<Boolean, String> {
        val pipelineName = "$PIPELINE_NAME_PREFIX$environment-$recordSetId"
        val description =
            "An ingest pipeline to update institutions and record set for records in record set $recordSetId.\n\n" +
                    ("$recordSetId,${recordSetName.de},${recordSetName.fr},${recordSetName.it}," +
                            "$institutions,$accessInstitutions,$masterInstitutions,$originalInstitutions").hashedWithSha256()
        try {
            val response = client.ingest().getPipeline(GetPipelineRequest.Builder().id(pipelineName).build())
            val pipeline = response.get(pipelineName)
            if (pipeline != null) {
                log.info("Pipeline $pipelineName exists.")
                log.info("Existing pipeline description: ${pipeline.description()}. New Pipeline description: $description.")
                if (pipeline.description() == description) {
                    log.info("No changes to pipeline $pipelineName needed.")
                    return Pair(
                        false,
                        "No changes to record set or institution titles made. Documents are not updated."
                    )
                }
            }
        } catch (ex: Exception) {
            log.error("Error checking if pipeline exists: ${ex.localizedMessage}")
            throw ElasticsearchException("Error checking if pipeline exists: ${ex.localizedMessage}")
        }
        return Pair(true, "Record set title or institution titles changed. Documents are updated.")
    }


    suspend fun updateByQuery(
        index: String,
        recordSetId: String,
        environment: String,
        recordSetName: LanguageContainer,
        institutions: List<FacetContainer>,
        accessInstitutions: List<FacetContainer>,
        masterInstitutions: List<FacetContainer>,
        originalInstitutions: List<FacetContainer>,
    ) {
        val pipelineName = "$PIPELINE_NAME_PREFIX$environment-$recordSetId"
        val description =
            "An ingest pipeline to update institutions and record set for records in record set $recordSetId.\n\n" +
                    ("$recordSetId,${recordSetName.de},${recordSetName.fr},${recordSetName.it}," +
                            "$institutions,$accessInstitutions,$masterInstitutions,$originalInstitutions").hashedWithSha256()
        try {
            val response = client.ingest().putPipeline { builder ->
                builder.id(pipelineName)
                    // this meta section is only here, because the library parser requires this field to be present in the response.
                    .meta(mapOf("recordSet" to JsonData.of(mapOf("recordSetId" to recordSetId))))
                    .description(description)
                    .processors(
                        listOf(
                            Processor.Builder().set {
                                it.field(DOCUMENT_FIELD_RECORD_SET).value(
                                    JsonData.of(
                                        mapOf(
                                            "name" to
                                                    mapOf(
                                                        "de" to recordSetName.de,
                                                        "fr" to recordSetName.fr,
                                                        "it" to recordSetName.it
                                                    ),
                                            "facet" to listOf(recordSetId)
                                        )
                                    )
                                )
                            }.build(),
                            Processor.Builder().set {
                                it.field(DOCUMENT_FIELD_INSTITUTION).value(JsonData.of(institutions))
                            }.build(),
                            Processor.Builder().set {
                                it.field(DOCUMENT_FIELD_ORIGINAL_INSTITUTION).value(JsonData.of(originalInstitutions))
                            }.build(),
                            Processor.Builder().set {
                                it.field(DOCUMENT_FIELD_MASTER_INSTITUTION).value(JsonData.of(masterInstitutions))
                            }.build(),
                            Processor.Builder().set {
                                it.field(DOCUMENT_FIELD_ACCESS_INSTITUTION).value(JsonData.of(accessInstitutions))
                            }.build()
                        )
                    )
            }
            if (response.acknowledged()) {
                log.info("Pipeline $pipelineName created.")
            } else {
                log.error("Pipeline $pipelineName creation failed.")
                throw ElasticsearchException("Pipeline $pipelineName creation failed.")
            }
        } catch (ex: Exception) {
            log.error("Error creating pipeline: ${ex.localizedMessage}")
            throw ElasticsearchException("Error creating pipeline: ${ex.localizedMessage}")
        }

        try {
            val response = client.updateByQuery { builder ->
                builder.index(index)
                    .pipeline(pipelineName)
                    .query { queryBuilder ->
                        queryBuilder.term { termBuilder ->
                            termBuilder.field(DOCUMENT_TERM_QUERY_FIELD_RECORD_SET).value(recordSetId)
                        }
                    }
                    .waitForCompletion(false)
            }
            val taskId = response.task()
            if (taskId == null) {
                log.error("Error updating by query: Task id is null.")
                throw ElasticsearchException("Error updating by query: Task id is null.")
            }
            waitForTaskCompletion(taskId)
        } catch (ex: Exception) {
            log.error("Error updating by query: ${ex.localizedMessage}")
            throw ElasticsearchException("Error updating by query: ${ex.localizedMessage}")
        }
    }

    private suspend fun waitForTaskCompletion(taskId: String) {
        while (true) {
            try {
                val response = client.tasks().get {
                    it.taskId(taskId)
                }
                if (response.completed()) {
                    log.info("Task $taskId completed.")
                    break
                }
            } catch (ex: Exception) {
                log.error("Error getting task info: ${ex.localizedMessage}")
                throw ElasticsearchException("Error getting task info: ${ex.localizedMessage}")
            }

            withContext(Dispatchers.IO) {
                Thread.sleep(WAIT_FOR_COMPLETION_SLEEP_DURATION_MS)
            }
        }
    }

    private fun buildSSLContext(path: String): SSLContext {
        val caCertificatePath = Paths.get(path)
        val factory = CertificateFactory.getInstance("X.509")
        var trustedCa: Certificate?
        val inputStream = try {
            Files.newInputStream(caCertificatePath)
        } catch (ex: IOException) {
            log.error("Could not open certificate file: $path")
            exitProcess(1)
        }
        inputStream.use { `is` -> trustedCa = factory.generateCertificate(`is`) }
        val trustStore = KeyStore.getInstance("pkcs12")
        trustStore.load(null, null)
        trustStore.setCertificateEntry("ca", trustedCa)
        val sslContextBuilder: SSLContextBuilder = SSLContexts.custom().loadTrustMaterial(trustStore, null)
        return sslContextBuilder.build()
    }

    private fun parseHost(hosts: String): Array<HttpHost> {
        return hosts.split(",").map { host ->
            val hostParts = hostRegex.find(host) ?: throw InvalidHostException("No match for host: $host")
            val scheme = hostParts.groups[1]?.value ?: throw InvalidHostException("No scheme captured for host: $host")
            val hostName =
                hostParts.groups[2]?.value ?: throw InvalidHostException("No domain captured for host: $host")
            val portGroupValue =
                hostParts.groups[3]?.value ?: throw InvalidHostException("No port captured for host: $host")
            val port = try {
                portGroupValue.toInt()
            } catch (ex: NumberFormatException) {
                throw InvalidHostException("Port is not a number for host: $host")
            }
            HttpHost(hostName, port, scheme)
        }.toTypedArray()
    }
}
