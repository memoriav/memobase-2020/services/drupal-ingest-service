/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.drupal

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class InstitutionInventarDataAttributes(
    val title: String,
    @SerialName("field_digital_number") val fieldDigitalCount: Int?,
    @SerialName("field_physical_number") val fieldPhysicalCount: Int?,
    @SerialName("field_digitized") val fieldDigitizedPercentage: Int?,
    @SerialName("field_indexed") val fieldIndexedPercentage: Int?,
)
