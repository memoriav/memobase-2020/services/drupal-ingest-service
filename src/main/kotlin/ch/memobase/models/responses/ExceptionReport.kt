/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.responses

import ch.memobase.utility.Date
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import java.io.Reader
import java.io.StringReader

@Serializable
data class ExceptionReport @OptIn(ExperimentalSerializationApi::class) constructor(
    val uuid: String,
    val id: String?,
    val exceptionClass: String,
    val exceptionMessage: String?,
    val stackTrace: String? = null,
    val type: String,
    @SerialName("@timestamp")
    @EncodeDefault
    val timestamp: String = Date.nowWithTime(),
) {
    fun getReader(): Reader {
        return StringReader(Json.encodeToString(this))
    }
}