/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models

import ch.memobase.utility.InvalidDataException
import kotlinx.serialization.Serializable

@Serializable
data class FacetContainer(
    var name: LanguageContainer = LanguageContainer(),
    var filter: String? = null,
    var facet: List<String>? = null
)

fun facetContainer(block: FacetContainer.() -> Unit): FacetContainer {
    return FacetContainer().apply(block)
}

fun language(code: String): FacetContainer {
    return when (code) {
        "de" -> facetContainer {
            name = LanguageContainer(
                "Deutsch",
                "Allemand",
                "Tedesco"
            )
            filter = code
        }

        "fr" -> facetContainer {
            name = LanguageContainer(
                "Französisch",
                "Français",
                "Francese"
            )
            filter = code
        }

        "it" -> facetContainer {
            name = LanguageContainer(
                "Italienisch",
                "Italien",
                "Italiano"
            )
            filter = code
        }

        else -> {
            throw InvalidDataException("Language code $code is not supported.")
        }
    }
}
