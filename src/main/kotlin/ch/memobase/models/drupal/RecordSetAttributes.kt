/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.drupal

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RecordSetAttributes(
    val title: String,
    val status: Boolean,
    val name: String? = null,
    @SerialName("field_memobase_id") val fieldMemobaseId: String,
    @SerialName("field_old_memobase_id") val fieldOldMemobaseId: String?,
    @SerialName("field_access") val fieldAccess: AttributeRichText?,
    @SerialName("field_access_memobase") val fieldAccessMemobase: AttributeRichText?,
    @SerialName("field_content") val fieldContent: AttributeRichText?,
    @SerialName("field_context") val fieldContext: AttributeRichText?,
    @SerialName("field_data_transfer") val fieldDataTransfer: AttributeRichText?,
    @SerialName("field_documents") val fieldDocuments: List<AttributeLink>,
    @SerialName("field_info_on_development") val fieldInfoOnDevelopment: AttributeRichText?,
    @SerialName("field_language") val fieldLanguage: String?,
    @SerialName("field_original_description") val fieldOriginalDescription: AttributeLink?,
    @SerialName("field_original_id") val fieldOriginalId: String?,
    @SerialName("field_original_shelf_mark") val fieldOriginalShelfMark: String?,
    @SerialName("field_original_title") val fieldOriginalTitle: String?,
    @SerialName("field_processed_teaser_text") val fieldProcessedTeaserText: AttributeRichText?,
    @SerialName("field_project") val fieldProject: List<AttributeLink>,
    @SerialName("field_publications") val fieldPublications: List<AttributeLink>,
    @SerialName("field_related_record_sets") val fieldRelatedRecordSets: List<AttributeLink>,
    @SerialName("field_rights") val fieldRights: AttributeRichText?,
    @SerialName("field_scope") val fieldScope: AttributeRichText?,
    @SerialName("field_selection") val fieldSelection: AttributeRichText?,
    @SerialName("field_supported_by_memoriav") val fieldSupportedByMemoriav: Boolean,
    @SerialName("field_time_period") val fieldTimePeriod: String?,
    @SerialName("field_transfer_date") val fieldTransferDate: String?,
    @SerialName("computed_teaser_image_url") val computedTeaserImageUrl: String?
)