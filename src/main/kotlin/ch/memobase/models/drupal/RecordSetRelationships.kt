/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.drupal

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RecordSetRelationships(
    @SerialName("field_image_gallery") val fieldImageGallery: RelationshipsData?,
    @SerialName("field_institution") val fieldInstitution: RelationshipsListData?,
    @SerialName("field_metadata_languages") val fieldMetadataLanguages: RelationshipsListData?,
    @SerialName("field_resp_institution_access") val fieldRespInstitutionAccess: RelationshipsListData?,
    @SerialName("field_resp_institution_master") val fieldRespInstitutionMaster: RelationshipsListData?,
    @SerialName("field_resp_institution_original") val fieldRespInstitutionOriginal: RelationshipsListData?
)