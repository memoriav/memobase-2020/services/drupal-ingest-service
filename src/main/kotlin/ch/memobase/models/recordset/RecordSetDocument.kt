/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.recordset

import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import ch.memobase.utility.Date
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.Reader
import java.io.StringReader

@Serializable
data class RecordSetDocument @OptIn(ExperimentalSerializationApi::class) constructor(
    var id: String = "",
    var published: Boolean = false,
    var name: LanguageContainer = LanguageContainer(),
    var drupalUuid: String = "",

    // Display
    var keyVisualLink: String? = null,
    var numberOfDocuments: Int = 0,
    var numberOfDocumentsPublished: Int = 0,
    var teaserText: LanguageContainer = LanguageContainer(),
    // Facets
    var documentType: List<FacetContainer> = emptyList(),
    var periodOfTimeAsYear: IntegerRange? = null,
    var periodOfTimeAsYearFrom: String? = null,
    var periodOfTimeAsYearTo: String? = null,
    var supportedByMemoriav: Boolean = false,
    var languageOfMetadata: List<FacetContainer> = emptyList(),
    var institution: List<FacetContainer> = emptyList(),

    // Sort
    @EncodeDefault
    var lastUpdatedDate: String = Date.now(),

    // Search
    var scopeAndContent: LanguageContainer? = null,
    var accessMemobase: LanguageContainer? = null,
    var context: LanguageContainer? = null,
    var originalTitle: LanguageContainer? = null,
    var extent: LanguageContainer? = null,
    var selection: LanguageContainer? = null,
    var indexing: LanguageContainer? = null,
    var rights: LanguageContainer? = null,
    var description: LanguageContainer? = null,
    var access: LanguageContainer? = null,
    var project: LanguageContainer? = null,
    var relatedRecordSets: LanguageContainer? = null,
    var relatedPublications: LanguageContainer? = null,
    var relatedDocuments: LanguageContainer? = null,
    var dataImport: LanguageContainer? = null,
    var originalCallNumber: String? = null,
    var originalIdNumber: String? = null,

    // Support Field (required to be indexed in the record).
    var originalInstitution: List<FacetContainer> = emptyList(),
    var masterInstitution: List<FacetContainer> = emptyList(),
    var accessInstitution: List<FacetContainer> = emptyList()
) {
    @OptIn(ExperimentalSerializationApi::class)
    fun getReader(): Reader {
        val json = Json {
            explicitNulls = false
        }
        return StringReader(json.encodeToString(this))
    }
}

fun recordSetDocument(block: RecordSetDocument.() -> Unit): RecordSetDocument {
    return RecordSetDocument().apply(block)
}

