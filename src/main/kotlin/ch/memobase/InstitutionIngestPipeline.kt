/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.impl.DrupalApiClient
import ch.memobase.impl.ElasticClient
import ch.memobase.impl.InstitutionDataCollectionPipeline
import ch.memobase.models.EnrichedFacetContainer
import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import ch.memobase.models.config.IndexConfiguration
import ch.memobase.models.data.InventoryDocumentTypeValue
import ch.memobase.models.data.LanguageCode
import ch.memobase.models.data.Municipality
import ch.memobase.models.drupal.DrupalApiListResponse
import ch.memobase.models.drupal.DrupalApiResponse
import ch.memobase.models.drupal.InventoryCollectionDrupalName
import ch.memobase.models.drupal.data.*
import ch.memobase.models.institution.*
import ch.memobase.models.recordset.*
import ch.memobase.models.responses.ExceptionReport
import ch.memobase.rdf.NS
import ch.memobase.utility.Date
import ch.memobase.utility.DrupalApiException
import ch.memobase.utility.ElasticsearchException
import ch.memobase.utility.InvalidDataException
import kotlinx.coroutines.coroutineScope
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import kotlin.math.roundToInt

class InstitutionIngestPipeline(
    private val drupalClient: DrupalApiClient,
    private val elasticClient: ElasticClient,
    private val indexConfig: IndexConfiguration,
    private val cantons: Map<String, FacetContainer>,
    private val municipalities: Map<String, List<Municipality>>,
    private val inventoryDocumentTypes: Map<InventoryDocumentTypeValue, FacetContainer>,
    private val inventoryGeo: Map<String, EnrichedFacetContainer>,
    private val inventoryTimePeriod: Map<String, EnrichedFacetContainer>,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val dataCollectionPipeline = InstitutionDataCollectionPipeline(drupalClient, elasticClient, indexConfig)

    init {
        with(elasticClient) {
            checkIndex(
                indexConfig.institutionsIndex,
                indexConfig.institutionsIndexMappingFilePath,
                indexConfig.institutionsIndexSettingFilePath
            )
            checkIndex(
                indexConfig.institutionApiIndex,
                indexConfig.institutionApiIndexMappingFilePath,
                indexConfig.institutionApiIndexSettingFilePath
            )
        }
    }

    suspend fun ingestAll() = coroutineScope {
        val institutions = drupalClient.getAllInstitutionUuids()
        institutions.forEach {
            ingest(it, "none")
        }
        log.info("Ingestion of all ${institutions.size} institutions completed.")
    }

    suspend fun ingest(uuid: String, memobaseId: String) {
        val institutionData = try {
            dataCollectionPipeline.collectInstitutionData(uuid)
        } catch (ex: DrupalApiException) {
            errorReporting(uuid, memobaseId, ex)
            return
        } catch (ex: ElasticsearchException) {
            errorReporting(uuid, memobaseId, ex)
            return
        } catch (ex: Exception) {
            errorReporting(uuid, memobaseId, ex)
            return
        }
        val institutionDataDe = institutionData.institutionData[LanguageCode.DE]?.data as InstitutionData
        val id = institutionDataDe.attributes.fieldMemobaseId
        val warning = if (institutionData.documentTypes.isEmpty()) {
            "There are no documents in the index for the ${institutionData.recordSetsCount} record sets of this institution."
        } else {
            ""
        }

        try {
            val (documentType, subjectData) = extractInventoryCollectionIncludes(institutionData.inventoryCollectionData)
            val (addresses, institutionTypes) = extractInstitutionIncludes(institutionData.institutionData)
            val institutionDocument =
                buildElasticsearchDocument(institutionData, documentType, subjectData, addresses, institutionTypes)
            val institutionJsonLdDocument =
                createJsonLdDocument(institutionData, documentType, subjectData, addresses, institutionTypes)
            elasticClient.indexDocument(
                indexConfig.institutionsIndex,
                institutionDocument.id,
                institutionDocument.getReader()
            )
            elasticClient.indexDocument(
                indexConfig.institutionApiIndex,
                NS.mbcb + institutionDocument.id,
                institutionJsonLdDocument.getReader()
            )

            if (warning.isNotBlank()) {
                elasticClient.indexDocument(
                    indexConfig.reportingIndex,
                    uuid,
                    ExceptionReport(
                        uuid,
                        id,
                        "Warning",
                        warning,
                        type = "Institution"
                    ).getReader()
                )
            } else {
                elasticClient.indexDocument(
                    indexConfig.reportingIndex,
                    uuid,
                    ExceptionReport(
                        uuid,
                        id,
                        "Success",
                        "Institution $id indexed successfully. The institution has ${institutionData.recordSetsCount} " +
                                "record sets with a total of ${institutionData.totalDocumentCount} (${institutionData.publishedDocumentCount} published).",
                        type = "Institution"
                    ).getReader()
                )
            }
        } catch (ex: ElasticsearchException) {
            errorReporting(uuid, id, ex)
        } catch (ex: InvalidDataException) {
            errorReporting(uuid, id, ex)
        } catch (ex: Exception) {
            errorReporting(uuid, id, ex)
        }
    }


    suspend fun ingestInventoryCollection(uuid: String) {
        val institutionUuid = try {
            drupalClient.getInventoryDataCollectionInstitutionId(uuid)
        } catch (ex: DrupalApiException) {
            errorReporting(uuid, null, ex, "InventoryCollection")
            return
        } catch (ex: Exception) {
            errorReporting(uuid, null, ex, "InventoryCollection")
            return
        }

        ingest(institutionUuid, "")
    }

    private fun errorReporting(uuid: String, id: String?, ex: Exception, type: String = "Institution") {
        elasticClient.indexDocument(
            indexConfig.reportingIndex,
            uuid,
            ExceptionReport(
                uuid,
                id,
                ex::class.java.name,
                ex.localizedMessage,
                type = type,
                stackTrace = ex.stackTraceToString()
            ).getReader()
        )
        log.error("Failed to import $type $id ($uuid): ${ex}.")
    }

    private fun buildElasticsearchDocument(
        input: InstitutionCompleteData,
        documentType: Map<String, InventoryDocumentTypeValue>,
        subjectData: Map<String, LanguageContainer>,
        addresses: List<InstitutionAddress>,
        institutionTypes: List<FacetContainer>,
    ): InstitutionDocument {
        val institutionDataDe = input.institutionData[LanguageCode.DE]?.data as InstitutionData
        if (addresses.isEmpty()) {
            throw InvalidDataException("No address found for institution ${institutionDataDe.attributes.fieldMemobaseId} (${institutionDataDe.id}).")
        }
        val institutionDataFr = input.institutionData[LanguageCode.FR]?.data as InstitutionData
        val institutionDataIt = input.institutionData[LanguageCode.IT]?.data as InstitutionData
        val isSurveyInstitution = institutionDataDe.attributes.fieldSurveyInstitution ?: false
        val inventoryCollections =
            processInventoryCollectionData(input.inventoryCollectionData, documentType, subjectData)

        return InstitutionDocument(
            id = institutionDataDe.attributes.fieldMemobaseId,
            drupalUuid = institutionDataDe.id,
            type = institutionTypes,
            numberOfDocuments = input.totalDocumentCount.toInt(),
            numberOfDocumentsPublished = input.publishedDocumentCount.toInt(),
            numberOfRecordSets = input.recordSetsCount,
            name = LanguageContainer(
                de = listOf(institutionDataDe.attributes.title),
                fr = listOf(institutionDataFr.attributes.title),
                it = listOf(institutionDataIt.attributes.title),
            ),
            description = LanguageContainer(
                de = listOfNotNull(institutionDataDe.attributes.fieldText?.value),
                fr = listOfNotNull(institutionDataFr.attributes.fieldText?.value),
                it = listOfNotNull(institutionDataIt.attributes.fieldText?.value),
            ),
            keyVisualLink = institutionDataDe.attributes.computedTeaserImageUrl,
            teaserColor = institutionDataDe.attributes.fieldTeaserColor,
            teaserColorComputed = institutionDataDe.attributes.computedTeaserColor,
            surveyInstitution = isSurveyInstitution,
            fullInstitutionPage = institutionDataDe.attributes.fieldMemobaseInstitution ?: false,
            published = institutionDataDe.attributes.status,
            documentType = input.documentTypes,
            private = institutionDataDe.attributes.fieldPrivate ?: true,
            lastUpdatedDate = Date.now(),
            mainAddress = addresses.first(),
            additionalAddresses = addresses.drop(1),
            combined = inventoryCollections[InventoryDocumentTypeValue.COMBINED],
            film = inventoryCollections[InventoryDocumentTypeValue.FILM],
            image = inventoryCollections[InventoryDocumentTypeValue.IMAGE],
            video = inventoryCollections[InventoryDocumentTypeValue.VIDEO],
            sound = inventoryCollections[InventoryDocumentTypeValue.SOUND],
            other = inventoryCollections[InventoryDocumentTypeValue.OTHER],
        )
    }

    private fun processInventoryCollectionData(
        inventoryCollectionData: Map<LanguageCode, DrupalApiListResponse>,
        documentType: Map<String, InventoryDocumentTypeValue>,
        subjectData: Map<String, LanguageContainer>,
    ): Map<InventoryDocumentTypeValue, InventoryCollection> {
        val inventoryCollections = mutableMapOf<InventoryDocumentTypeValue, InventoryCollection>()
        inventoryCollectionData[LanguageCode.DE]?.data?.forEach { inventoryData ->
            val data = inventoryData as InstitutionInventarData
            val type = inventoryData.relationships.fieldDocumentType?.data
            if (type == null) {
                log.warn("No document type found for inventory collection ${data.attributes.title}.")
                return@forEach
            }
            val inventoryDocumentType = documentType[type.id]
                ?: throw DrupalApiException("No document type found for inventory collection ${data.attributes.title}.")
            val inventoryDocumentTypeFacetContainer =
                inventoryDocumentTypes[inventoryDocumentType] ?: throw DrupalApiException(
                    "No document type found for inventory collection ${data.attributes.title}."
                )

            val inventoryCollection = InventoryCollection(
                indexed = data.attributes.fieldIndexedPercentage ?: 0,
                digitized = data.attributes.fieldDigitizedPercentage ?: 0,
                indexedAnswered = data.attributes.fieldIndexedPercentage != null,
                digitizedAnswered = data.attributes.fieldDigitizedPercentage != null,
                numberOfObjects = NumberOfObjects(
                    physical = data.attributes.fieldPhysicalCount ?: 0,
                    digital = data.attributes.fieldDigitalCount ?: 0
                ),
                geo = data.relationships.fieldGeo?.data?.mapNotNull { inventoryGeo[it.id] }?.map {
                    // the type is used to store the drupal UUID and doesn't need to be searchable.
                    it.type = null
                    it
                } ?: emptyList(),
                subjects = data.relationships.fieldSubjects?.data?.mapNotNull { subjectData[it.id] } ?: emptyList(),
                timePeriod = data.relationships.fieldTime?.data?.mapNotNull { inventoryTimePeriod[it.id] }?.map {
                    // the type is used to store the drupal UUID and doesn't need to be searchable.
                    it.type = null
                    it
                }
                    ?: emptyList(),
                documentType = listOf(inventoryDocumentTypeFacetContainer),
            )
            inventoryCollections[inventoryDocumentType] = inventoryCollection
        }


        val collections = inventoryCollections.values
        if (collections.isNotEmpty()) {
            val totalPhysical = collections.filter { it.digitizedAnswered }.sumOf { it.numberOfObjects.physical }
            val totalAll = collections.filter { it.indexedAnswered }.sumOf { it.numberOfObjects.total }
            val physicalDigitized = collections.filter { it.digitizedAnswered }
                .sumOf { it.numberOfObjects.physical * (it.digitized.toDouble() / 100.0) }
            val totalIndexed = collections.filter { it.indexedAnswered }
                .sumOf { it.numberOfObjects.total * (it.indexed.toDouble() / 100.0) }
            val roundedAverageDigitized = if (totalPhysical > 0) {
                (((physicalDigitized / totalPhysical) * 100.0) / 20.0).roundToInt() * 20
            } else {
                0
            }
            val roundedAverageIndexed =
                if (totalAll >= 0) (((totalIndexed / totalAll) * 100.0) / 20.0).roundToInt() * 20
                else 0
            inventoryCollections[InventoryDocumentTypeValue.COMBINED] =
                collections.reduce { acc, inventoryCollection ->
                    InventoryCollection(
                        geo = (acc.geo + inventoryCollection.geo).distinctBy { it.displayLabel.de.first() },
                        subjects = (acc.subjects + inventoryCollection.subjects).distinctBy { it.de.first() },
                        timePeriod = (acc.timePeriod + inventoryCollection.timePeriod).distinctBy { it.displayLabel.de.first() },
                        numberOfObjects = acc.numberOfObjects.combine(inventoryCollection.numberOfObjects),
                        documentType = acc.documentType + inventoryCollection.documentType,
                        digitized = roundedAverageDigitized,
                        digitizedAnswered = acc.digitizedAnswered || inventoryCollection.digitizedAnswered,
                        indexed = roundedAverageIndexed,
                        indexedAnswered = acc.digitizedAnswered || inventoryCollection.digitizedAnswered
                    )
                }

        }
        return inventoryCollections
    }


    private fun createJsonLdDocument(
        completeData: InstitutionCompleteData,
        documentType: Map<String, InventoryDocumentTypeValue>,
        subjectData: Map<String, LanguageContainer>,
        addresses: List<InstitutionAddress>,
        institutionTypes: List<FacetContainer>,
    ): InstitutionJsonLdDocument {
        val institutionDataDe = completeData.institutionData[LanguageCode.DE]?.data as InstitutionData
        val institutionDataFr = completeData.institutionData[LanguageCode.FR]?.data as InstitutionData
        val institutionDataIt = completeData.institutionData[LanguageCode.IT]?.data as InstitutionData

        val institutionUri = NS.mbcb + institutionDataDe.attributes.fieldMemobaseId

        val avkIdentifier = institutionDataDe.attributes.fieldMemobaseId + "-avk"
        val avkUri = NS.mbrs + avkIdentifier



        return InstitutionJsonLdDocument(
            id = institutionUri,
            ricoType = if (institutionDataDe.attributes.fieldMemobaseInstitution == true)
                InstitutionTypeValue.MEMOBASE_INSTITUTION
            else
                InstitutionTypeValue.MEMOBASE_INSTITUTION_INVENTORY_PROJECT,
            nameDe = institutionDataDe.attributes.title,
            nameFr = institutionDataFr.attributes.title,
            nameIt = institutionDataIt.attributes.title,
            sameAs = institutionDataDe.attributes.fieldWikidataId?.uri,
            descriptiveNoteDe = institutionDataDe.attributes.fieldText?.value,
            descriptiveNoteFr = institutionDataFr.attributes.fieldText?.value,
            descriptiveNoteIt = institutionDataIt.attributes.fieldText?.value,
            isil = institutionDataDe.attributes.fieldIsil,
            url = institutionDataDe.attributes.fieldLinkArchiveCatalog?.uri,
            officialWebsite = institutionDataDe.attributes.fieldWebsite?.uri,
            emailAddress = run {
                val email = institutionDataDe.attributes.fieldEmail
                if (email != null) {
                    "mailto:$email"
                } else {
                    null
                }
            },
            image = institutionDataDe.attributes.computedTeaserImageUrl,
            isPublished = institutionDataDe.attributes.status,
            instanceOf = institutionTypes.mapNotNull { it.filter },
            hasOrHadIdentifier = RicoIdentifier(
                type = IdentifierType.MAIN,
                identifier = institutionDataDe.attributes.fieldMemobaseId,
                isOrWasIdentifierOf = institutionUri
            ),

            hasOrHadLocation = addresses.map { address ->
                val addressParts = address.street.split(" ")
                val locatedOnStreet = addressParts.dropLast(1).joinToString(" ")
                val streetNumber = addressParts.last()
                val municipality = address.city

                val adminEntities = mutableListOf<AdministrativeTerritorialEntity>()
                adminEntities.add(
                    AdministrativeTerritorialEntity(
                        nameDe = municipality.de.first(),
                        nameFr = municipality.fr.first(),
                        nameIt = municipality.it.first(),
                        sameAs = address.cityWikidataId,
                        type = AdministrativeTerritorialEntityTypeValue.MUNICIPALITY,
                    )
                )
                adminEntities.add(
                    AdministrativeTerritorialEntity(
                        nameDe = address.canton.name.de.firstOrNull(),
                        nameFr = address.canton.name.fr.firstOrNull(),
                        nameIt = address.canton.name.it.firstOrNull(),
                        sameAs = address.canton.facet?.firstOrNull(),
                        type = AdministrativeTerritorialEntityTypeValue.CANTON,
                    )
                )
                RicoPlace(
                    ricoType = RicoPlaceTypeValue.MAIN_ADDRESS,
                    postalCode = address.postalCode,
                    streetAddress = address.street,
                    locatedOnStreet = locatedOnStreet,
                    streetNumber = streetNumber,
                    country = "https://www.wikidata.org/wiki/Q39",
                    coordinateLocation = address.coordinates.latitude + ", " + address.coordinates.longitude,
                    isOrWasLocationOf = institutionUri,
                    locatedInTheAdministrativeTerritorialEntity = adminEntities
                )
            },
            isOrWasHolderOf = MemobaseSurveyRecordSetParent(
                id = avkIdentifier,
                titleDe = "Sammlung Audiovisuelles Kulturgut " + institutionDataDe.attributes.title,
                titleFr = "Collection de biens culturels audiovisuels " + institutionDataFr.attributes.title,
                titleIt = "Collezione di beni culturali audiovisivi " + institutionDataIt.attributes.title,
                publicationPermitted = institutionDataDe.attributes.fieldPrivate ?: false,
                publicationDate = institutionDataDe.attributes.fieldSurveyDate,
                hasOrHadTitle = RicoTitle(
                    titleDe = "Sammlung Audiovisuelles Kulturgut " + institutionDataDe.attributes.title,
                    titleFr = "Collection de biens culturels audiovisuels " + institutionDataFr.attributes.title,
                    titleIt = "Collezione di beni culturali audiovisivi " + institutionDataIt.attributes.title,
                    isOrWasTitleOf = avkUri,
                    type = TitleType.MAIN,
                ),
                hasOrHadIdentifier = RicoIdentifier(
                    type = IdentifierType.MAIN,
                    identifier = avkIdentifier,
                    isOrWasIdentifierOf = avkUri
                ),
                hasOrHadHolder = institutionUri,
                hasOrHadPart = completeData.inventoryCollectionData[LanguageCode.DE]?.data?.mapNotNull { inventoryCollectionData ->
                    if (inventoryCollectionData is InstitutionInventarData) {
                        val field =
                            inventoryCollectionData.relationships.fieldDocumentType?.data ?: return@mapNotNull null
                        val collectionContentType =
                            documentType[field.id]
                                ?: throw InvalidDataException("No document type found for inventory collection ${inventoryCollectionData.attributes.title} (${inventoryCollectionData.id}).")
                        val locations =
                            inventoryCollectionData.relationships.fieldGeo?.data?.mapNotNull { inventoryGeo[it.id] }
                                ?: emptyList()

                        val identifier =
                            institutionDataDe.attributes.fieldMemobaseId + "-" + collectionContentType.name.lowercase()
                        val compactedUri = NS.mbrs + ":" + identifier
                        MemobaseSurveyRecordSet(
                            id = identifier,
                            titleDe = inventoryCollectionData.attributes.title,
                            titleFr = inventoryCollectionData.attributes.title,
                            titleIt = inventoryCollectionData.attributes.title,
                            isOrWasPartOf = avkUri,
                            hasOrHadIdentifier = RicoIdentifier(
                                type = IdentifierType.MAIN,
                                identifier = identifier,
                                isOrWasIdentifierOf = compactedUri
                            ),
                            hasOrHadTitle = RicoTitle(
                                titleDe = inventoryCollectionData.attributes.title,
                                titleFr = inventoryCollectionData.attributes.title,
                                titleIt = inventoryCollectionData.attributes.title,
                                isOrWasTitleOf = compactedUri,
                                type = TitleType.MAIN,
                            ),
                            isAssociatedWithDate = inventoryCollectionData.relationships.fieldTime?.data?.mapNotNull {
                                inventoryTimePeriod[it.id]?.let { timePeriod ->
                                    val normalizedDateDe =
                                        normalizeInventoryCollectionTimePeriodDate(timePeriod.displayLabel.de.first())
                                    val normalizedDateFr =
                                        normalizeInventoryCollectionTimePeriodDate(timePeriod.displayLabel.fr.first())
                                    val normalizedDateIt =
                                        normalizeInventoryCollectionTimePeriodDate(timePeriod.displayLabel.it.first())
                                    RicoDate(
                                        normalizedDateValue = normalizedDateDe.first,
                                        dateQualifierDe = normalizedDateDe.second,
                                        dateQualifierFr = normalizedDateFr.second,
                                        dateQualifierIt = normalizedDateIt.second,
                                        isDateAssociatedWith = compactedUri,
                                    )
                                }
                            } ?: emptyList(),
                            hasOrHadSubject = inventoryCollectionData.relationships.fieldSubjects?.data?.mapNotNull {
                                subjectData[it.id]?.let { subject ->
                                    SkosConcept(
                                        prefLabelDe = subject.de.first(),
                                        prefLabelFr = subject.fr.first(),
                                        prefLabelIt = subject.it.first(),
                                        isOrWasSubjectOf = compactedUri
                                    )
                                }
                            } ?: emptyList(),
                            hasExtent = run {
                                val result = mutableListOf<RicoExtent>()
                                val physical = inventoryCollectionData.attributes.fieldPhysicalCount?.toString()
                                if (physical != null) {
                                    result.add(
                                        RicoExtent(
                                            quantity = physical,
                                            ricoType = RicoExtentTypeValue.PHYSICAL_OBJECTS,
                                            unitOfMeasurement = RicoExtentUnitOfMeasurementValue.AMOUNT_ESTIMATED,
                                            isExtentOf = compactedUri
                                        )
                                    )
                                }
                                val digital = inventoryCollectionData.attributes.fieldDigitalCount?.toString()
                                if (digital != null) {
                                    result.add(
                                        RicoExtent(
                                            quantity = digital,
                                            ricoType = RicoExtentTypeValue.DIGITAL_OBJECTS,
                                            unitOfMeasurement = RicoExtentUnitOfMeasurementValue.AMOUNT_ESTIMATED,
                                            isExtentOf = compactedUri
                                        )
                                    )
                                }
                                val digitized =
                                    inventoryCollectionData.attributes.fieldDigitizedPercentage?.toString()
                                if (digitized != null) {
                                    result.add(
                                        RicoExtent(
                                            quantity = digitized,
                                            ricoType = RicoExtentTypeValue.PHYSICAL_OBJECTS_DIGITIZED,
                                            unitOfMeasurement = RicoExtentUnitOfMeasurementValue.PERCENTAGE_ESTIMATED,
                                            isExtentOf = compactedUri
                                        )
                                    )
                                }
                                val indexed = inventoryCollectionData.attributes.fieldIndexedPercentage?.toString()
                                if (indexed != null) {
                                    result.add(
                                        RicoExtent(
                                            quantity = indexed,
                                            ricoType = RicoExtentTypeValue.OBJECTS_CATALOGUED,
                                            unitOfMeasurement = RicoExtentUnitOfMeasurementValue.PERCENTAGE_ESTIMATED,
                                            isExtentOf = compactedUri
                                        )
                                    )
                                }
                                result
                            },
                            hasCoverageOfContentDe = locations.map { it.displayLabel.de }.flatten(),
                            hasCoverageOfContentFr = locations.map { it.displayLabel.fr }.flatten(),
                            hasCoverageOfContentIt = locations.map { it.displayLabel.it }.flatten(),
                            hasOrHadAllMembersWithContentType = RicoContentType.fromType(
                                collectionContentType,
                                compactedUri
                            ),
                        )
                    } else {
                        null
                    }
                } ?: emptyList()
            ))
    }

    private fun extractInventoryCollectionIncludes(inventoryCollectionData: Map<LanguageCode, DrupalApiListResponse>):
            Pair<Map<String, InventoryDocumentTypeValue>, Map<String, LanguageContainer>> {
        val subjectData = mutableMapOf<String, LanguageContainer>()
        val documentType = mutableMapOf<String, InventoryDocumentTypeValue>()

        inventoryCollectionData.map { (language, drupalResponse) ->
            drupalResponse.included?.map { inventoryIncluded ->
                when (inventoryIncluded) {
                    is DocumentTypeData -> {
                        documentType[inventoryIncluded.id] = when (inventoryIncluded.attributes.name) {
                            InventoryCollectionDrupalName.FILM -> InventoryDocumentTypeValue.FILM
                            InventoryCollectionDrupalName.VIDEO -> InventoryDocumentTypeValue.VIDEO
                            InventoryCollectionDrupalName.TON -> InventoryDocumentTypeValue.SOUND
                            InventoryCollectionDrupalName.TON_IT -> InventoryDocumentTypeValue.SOUND
                            InventoryCollectionDrupalName.TON_FR -> InventoryDocumentTypeValue.SOUND
                            InventoryCollectionDrupalName.FOTO -> InventoryDocumentTypeValue.IMAGE
                            InventoryCollectionDrupalName.FOTO_FR -> InventoryDocumentTypeValue.IMAGE
                            else -> InventoryDocumentTypeValue.OTHER
                        }
                    }

                    is GeoData -> {
                        // is loaded from file
                    }

                    is TimeData -> {
                        // is loaded from file
                    }

                    is SubjectData -> {
                        val name = subjectData[inventoryIncluded.id]
                        if (name == null) {
                            subjectData[inventoryIncluded.id] =
                                LanguageContainer().addValue(language, inventoryIncluded.attributes.name)
                        } else {
                            subjectData[inventoryIncluded.id] =
                                name.addValue(language, inventoryIncluded.attributes.name)
                        }
                    }

                    else -> null
                }
            }
        }

        return Pair(documentType, subjectData)
    }

    private fun extractInstitutionIncludes(institutionData: Map<LanguageCode, DrupalApiResponse>): Pair<List<InstitutionAddress>, List<FacetContainer>> {

        val addresses = mutableListOf<InstitutionAddress>()
        val institutionTypes = mutableMapOf<String, FacetContainer>()

        institutionData.forEach { (language, response) ->
            response.included.forEach { data ->
                when (data) {
                    is ExtendedAddress -> {
                        if (language == LanguageCode.DE) {
                            val addressField = data.attributes.fieldAddress
                                ?: throw InvalidDataException("Address field is missing for institution ${(response.data as InstitutionData).attributes.fieldMemobaseId} (${response.data.id}).")
                            val coordinateValues =
                                data.attributes.fieldCoordinates.value.split(",").map { it.trim() }
                            val municipalities = municipalities[addressField.postalCode]
                                ?: throw InvalidDataException("Municipality ${addressField.locality} with postal code ${addressField.postalCode} was not found in list.")
                            val municipality = if (municipalities.size == 1) {
                                municipalities.first().toFacetContainer()
                            } else {
                                val names = mutableListOf<String>()
                                for (municipality in municipalities) {
                                    names.add(municipality.de)
                                    names.addAll(municipality.deAliases.split("|"))
                                    names.add(municipality.fr)
                                    names.addAll(municipality.frAliases.split("|"))
                                    names.add(municipality.it)
                                    names.addAll(municipality.itAliases.split("|"))
                                }
                                val nameList = names.filter { it.isNotBlank() }.joinToString(", ")
                                municipalities.firstOrNull {
                                    (it.de + it.deAliases + it.fr + it.frAliases + it.it + it.itAliases).contains(
                                        addressField.locality
                                    )
                                }.let {
                                    if (it != null) {
                                        it.toFacetContainer()
                                    } else {
                                        throw InvalidDataException("Municipality ${addressField.locality} with postal code ${addressField.postalCode} did not match any official or alternative names: $nameList")

                                    }
                                }
                            }
                            val address = InstitutionAddress(
                                postalCode = addressField.postalCode,
                                street = addressField.addressLine1,
                                canton = cantons[addressField.administrativeArea]
                                    ?: throw DrupalApiException("Canton '${addressField.administrativeArea}' is not a valid canton code."),
                                city = municipality.name,
                                cityWikidataId = municipality.filter,
                                coordinates = Coordinates(
                                    latitude = coordinateValues[0], longitude = coordinateValues[1]
                                )
                            )
                            addresses.add(address)
                        }
                    }

                    is InstitutionType -> {
                        val institutionTypeId = data.id
                        val institutionType = institutionTypes[institutionTypeId]
                        if (institutionType == null) {
                            institutionTypes[institutionTypeId] = FacetContainer(
                                name = LanguageContainer().addValue(language, data.attributes.name),
                                filter = data.attributes.fieldWikidata?.uri
                                    ?: throw DrupalApiException("Wikidata URI is missing for institution type ${data.attributes.name} in ${data.attributes.languageCode}.")
                            )
                        } else {
                            institutionTypes[institutionTypeId] = FacetContainer(
                                name = institutionType.name.addValue(language, data.attributes.name),
                                filter = institutionType.filter
                            )
                        }
                    }

                    else -> log.warn("Unknown type ${data.type} in included data.")
                }
            }
        }

        return Pair(addresses, institutionTypes.values.toList())
    }

    private fun normalizeInventoryCollectionTimePeriodDate(date: String): Pair<String, String?> {
        val dateParts = date.split("-")
        return when (dateParts.size) {
            1 -> {
                val dateWithQuantity = dateParts.first().split(" ")
                when (dateWithQuantity.size) {
                    2 -> Pair(dateWithQuantity[1], dateWithQuantity[0])
                    3 -> Pair(dateWithQuantity[2], dateWithQuantity[0] + " " + dateWithQuantity[1])
                    else -> throw IllegalArgumentException("Invalid date format: $date")
                }
            }

            2 -> Pair(dateParts[0] + "/" + dateParts[1], null)
            else -> throw IllegalArgumentException("Invalid date format: $date")
        }
    }
}