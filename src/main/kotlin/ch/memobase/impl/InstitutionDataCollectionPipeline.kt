/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.models.FacetContainer
import ch.memobase.models.config.IndexConfiguration
import ch.memobase.models.data.LanguageCode
import ch.memobase.models.institution.InstitutionCompleteData
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import org.apache.logging.log4j.LogManager


class InstitutionDataCollectionPipeline(
    private val client: DrupalApiClient,
    private val elasticClient: ElasticClient,
    private val indexConfig: IndexConfiguration,
) {
    private val log = LogManager.getLogger(this::class.java)
    suspend fun collectInstitutionData(uuid: String): InstitutionCompleteData = coroutineScope {
        val deDeferred = async { client.getInstitution(uuid, "de") }
        val frDeferred = async { client.getInstitution(uuid, "fr") }
        val itDeferred = async { client.getInstitution(uuid, "it") }
        val inventoryDataCollectionsDeDeferred = async { client.getInventoryDataCollections(uuid, "de") }
        val inventoryDataCollectionsFrDeferred = async { client.getInventoryDataCollections(uuid, "fr") }
        val inventoryDataCollectionsItDeferred = async { client.getInventoryDataCollections(uuid, "it") }
        val recordSetIdsDeferred = async { client.getRecordSetIdsWithFilter(uuid) }
        val recordSetIds = recordSetIdsDeferred.await()
        val documentDataDeferred = async { documentIndexData(recordSetIds) }
        val documentDataResult = documentDataDeferred.await()
        InstitutionCompleteData(
            mapOf(
                LanguageCode.DE to deDeferred.await(),
                LanguageCode.FR to frDeferred.await(),
                LanguageCode.IT to itDeferred.await()
            ),
            mapOf(
                LanguageCode.DE to inventoryDataCollectionsDeDeferred.await(),
                LanguageCode.FR to inventoryDataCollectionsFrDeferred.await(),
                LanguageCode.IT to inventoryDataCollectionsItDeferred.await()
            ),
            recordSetIds.size,
            documentDataResult.first,
            documentDataResult.second,
            documentDataResult.third
        )
    }

    private suspend fun documentIndexData(recordSetIds: List<String>): Triple<Long, Long, List<FacetContainer>> =
        coroutineScope {
            val totalDocumentCounts = recordSetIds.map { recordSetId ->
                async { elasticClient.countTotalNumberOfDocuments(recordSetId, indexConfig.documentsIndex) }
            }
            val unpublishedDocumentCounts = recordSetIds.map { recordSetId ->
                async { elasticClient.countNumberOfDocumentsUnpublished(recordSetId, indexConfig.documentsIndex) }
            }
            val allDocumentTypes = recordSetIds.map { recordSetId ->
                async { elasticClient.getDocumentTypesFromRecords(recordSetId, indexConfig.documentsIndex) }
            }.map { it.await() }.flatten().distinctBy { it.filter }
            val total = totalDocumentCounts.sumOf { it.await() }
            val unpublished = unpublishedDocumentCounts.sumOf { it.await() }
            Triple(total, total - unpublished, allDocumentTypes)
        }
}
