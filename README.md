# Memobase Ingest Service
This service offers an API to ingest record sets and 
institutions into the Memobase backend.

OpenAPI Specification: [documentation.yaml](src/main/resources/openapi/documentation.yaml)
Documentation Link: [Swagger UI](https://ingest-service.memobase-prod.k8s.unibas.ch/swagger/index.html)

## Development
The service is built with the specification in the Dockerfile.

### Build
```shell
docker build -t memobase-ingest-service .
```

### Run
```shell
docker run -p 8080:8080 memobase-ingest-service
```

## Deployment
The service is deployed to the Kubernetes cluster of the University of Basel.


### Test Requests

```sh
curl -X PUT http://0.0.0.0:5000/api/v1/institution/all
````

#### Get Response from Drupal API for specific institution
```shell
https://test.memobase.ch/de/jsonapi/node/institution/3231911e-5f4a-4b2d-b703-5d53c230fff3?include=field_extended_address,field_institution_types,field_logo,field_other_institution_types
```

## Municipalities Query
Use the [Wikidata Query Service](https://query.wikidata.org/) to get the municipalities of Switzerland.

```sparql
SELECT ?postalCode ?id (SAMPLE(?de) as ?de) (SAMPLE(?fr) as ?fr) (SAMPLE(?it) as ?it) 
WHERE 
{
  ?id wdt:P31 wd:Q70208. # swiss municipalities
  ?id wdt:P281 ?postalCode .
  
  ?id rdfs:label ?de .
  FILTER(LANG(?de) = "de")
  
  ?id rdfs:label ?fr .
  FILTER(LANG(?fr) = "fr")
  
  ?id rdfs:label ?it .
  FILTER(LANG(?it) = "it")
  
  MINUS {
    ?id wdt:P31 wd:Q685309 . # former swiss municipalities
  }
}
GROUP BY ?postalCode ?id
ORDER BY ?postalCode
```


To get a full list of places with a postal code, use the following query:

```sparql
SELECT ?postalCode ?id (SAMPLE(?de) as ?de) (SAMPLE(?fr) as ?fr) (SAMPLE(?it) as ?it) 
WHERE 
{
  {
    ?id wdt:P31 wd:Q11183787 . # Ortschaft (PLZ Admin)
    ?id wdt:P17 wd:Q39 . # in Switzerland
  } UNION {
    ?id wdt:P31 wd:Q70208. # swiss municipalities
  } UNION {
    ?id wdt:P31 wd:Q685309 . # former swiss municipalities
  }
  ?id wdt:P281 ?postalCode .
  
  ?id rdfs:label ?de .
  FILTER(LANG(?de) = "de")
  
  ?id rdfs:label ?fr .
  FILTER(LANG(?fr) = "fr")
  
  ?id rdfs:label ?it .
  FILTER(LANG(?it) = "it")
}
GROUP BY ?postalCode ?id
ORDER BY ?postalCode ?id
```

This includes former municipalities and Ortschaften. Ortschaften are the localities 
that are used for postal code administration in Switzerland. However, they are not often used on Wikidata.

The below query includes aliases for the names of the places:
```sparql
SELECT ?postalCode ?id 
       (SAMPLE(?de) as ?de) 
       (GROUP_CONCAT(DISTINCT ?deAlias; separator="|") as ?deAliases) 
       (SAMPLE(?fr) as ?fr) 
       (GROUP_CONCAT(DISTINCT ?frAlias; separator="|") as ?frAliases) 
       (SAMPLE(?it) as ?it) 
       (GROUP_CONCAT(DISTINCT ?itAlias; separator="|") as ?itAliases) 
WHERE 
{
  {
    ?id wdt:P31 wd:Q11183787 . # Ortschaft (PLZ Admin)
    ?id wdt:P17 wd:Q39 . # in Switzerland
  } UNION {
    ?id wdt:P31 wd:Q70208. # swiss municipalities
  } UNION {
    ?id wdt:P31 wd:Q685309 . # former swiss municipalities
  }
  ?id wdt:P281 ?postalCode .
  
  ?id rdfs:label ?de .
  FILTER(LANG(?de) = "de")
  
  ?id rdfs:label ?fr .
  FILTER(LANG(?fr) = "fr")
  
  ?id rdfs:label ?it .
  FILTER(LANG(?it) = "it")

  OPTIONAL {
    ?id skos:altLabel ?deAlias .
    FILTER(LANG(?deAlias) = "de")
  }
  
  OPTIONAL {
    ?id skos:altLabel ?frAlias .
    FILTER(LANG(?frAlias) = "fr")
  }
  
  OPTIONAL {
    ?id skos:altLabel ?itAlias .
    FILTER(LANG(?itAlias) = "it")
  }
}
GROUP BY ?postalCode ?id
ORDER BY ?postalCode ?id
```

It is possible that this still has to be expanded!

[Index of the official Ortschaften in switzerland](https://map.geo.admin.ch/?lang=en&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.swisstopo.zeitreihen,ch.bfs.gebaeude_wohnungs_register,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege,ch.astra.wanderland-sperrungen_umleitungen,ch.swisstopo-vd.ortschaftenverzeichnis_plz&layers_opacity=1,1,1,0.8,0.8,0.75&layers_visibility=false,false,false,false,false,true&layers_timestamp=18641231,,,,,&E=2736019.12&N=1250535.09&zoom=5)

Exploration to see where data is missing:

```sparql
SELECT ?postalCode ?id (SAMPLE(?de) as ?de) (SAMPLE(?fr) as ?fr) (SAMPLE(?it) as ?it) 
WHERE 
{
  {
    ?id wdt:P31 wd:Q11183787 . # Ortschaft (PLZ Admin)
    ?id wdt:P17 wd:Q39 . # in Switzerland
  } UNION {
    ?id wdt:P31 wd:Q70208. # swiss municipalities
  } UNION {
    ?id wdt:P31 wd:Q685309 . # former swiss municipalities
  }
  
  OPTIONAL {
    ?id wdt:P281 ?postalCode .
  }
  OPTIONAL {
    ?id rdfs:label ?de .
    FILTER(LANG(?de) = "de") 
  }
  OPTIONAL {
    ?id rdfs:label ?fr .
    FILTER(LANG(?fr) = "fr")
  }
  OPTIONAL {
    ?id rdfs:label ?it .
    FILTER(LANG(?it) = "it")
  }
}
GROUP BY ?postalCode ?id
ORDER BY ?postalCode ?id
```


## To Update the Geo and Time Period Taxonomy UUIDs

- [Geo (Prod)](https://memobase.ch/de/jsonapi/taxonomy_term/geo)
- [Geo (Stage)](https://stage.memobase.ch/de/jsonapi/taxonomy_term/geo)
- [Geo (Test)](https://test.memobase.ch/de/jsonapi/taxonomy_term/geo)
- [TimePeriod (Prod)](https://memobase.ch/de/jsonapi/taxonomy_term/time)
- [TimePeriod (Stage)](https://stage.memobase.ch/de/jsonapi/taxonomy_term/time)
- [TimePeriod (Test)](https://test.memobase.ch/de/jsonapi/taxonomy_term/time)