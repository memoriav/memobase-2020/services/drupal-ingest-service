/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.recordset

import ch.memobase.rdf.MB
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import java.io.Reader
import java.io.StringReader

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RecordSetJsonLd(
    @EncodeDefault @SerialName("@context") val context: String = "https://memobase.ch/contexts/recordSet.json",
    @SerialName("@id")
    val id: String,
    @EncodeDefault @SerialName("@type") val type: RdfType = RdfType.RECORD_SET,
    val conditionsOfAccessDe: String?,
    val conditionsOfAccessFr: String?,
    val conditionsOfAccessIt: String?,
    val conditionsOfUseDe: String?,
    val conditionsOfUseFr: String?,
    val conditionsOfUseIt: String?,
    val conformsToDe: String?,
    val conformsToFr: String?,
    val conformsToIt: String?,
    val descriptiveNoteDe: String?,
    val descriptiveNoteFr: String?,
    val descriptiveNoteIt: String?,
    val hasLanguageOfResourceDe: String?,
    val hasLanguageOfResourceFr: String?,
    val hasLanguageOfResourceIt: String?,
    val hasOrHadHolder: List<String>,
    val hasOrHadIdentifier: List<RicoIdentifier>,
    val hasOrHadLanguage: List<RicoLanguage>,
    val hasOrHadTitle: List<RicoTitle>,
    val hasRestrictionOnAccessDe: String?,
    val hasRestrictionOnAccessFr: String?,
    val hasRestrictionOnAccessIt: String?,
    val hasSource: RicoSource?,
    val hasSponsoringAgentOfResource: List<SponsoringAgent>,
    val historyDe: String?,
    val historyFr: String?,
    val historyIt: String?,
    val image: String?,
    val integrityDe: String?,
    val integrityFr: String?,
    val integrityIt: String?,
    val isAssociatedWithDate: RicoDate?,
    val isOrWasSubjectOf: List<RicoSubjectRecord>,
    val isPublished: Boolean,
    val isRecordResourceAssociatedWithRecordResource: List<RicoRecordResource>,
    val modificationDate: String,
    val publicationDate: String?,
    val recordResourceExtentDe: String?,
    val recordResourceExtentFr: String?,
    val recordResourceExtentIt: String?,
    val recordResourceOrInstantiationIsTargetOfRecordResourceHoldingRelation: List<RicoRecordResourceHoldingRelation>,
    val scopeAndContentDe: String?,
    val scopeAndContentFr: String?,
    val scopeAndContentIt: String?,
    val titleDe: String,
    val titleFr: String,
    val titleIt: String,
) {
    @OptIn(ExperimentalSerializationApi::class)
    fun getReader(): Reader {
        val json = Json {
            explicitNulls = false
        }
        return StringReader(json.encodeToString(this))
    }
}

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoIdentifier(
    @EncodeDefault
    @SerialName("@type") val rdfType: RdfType = RdfType.IDENTIFIER,
    val type: IdentifierType,
    val identifier: String,
    val isOrWasIdentifierOf: String
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoLanguage(
    @EncodeDefault
    @SerialName("@type") val rdfType: RdfType = RdfType.LANGUAGE,
    @EncodeDefault val type: String = "metadata",
    val nameDe: String,
    val nameFr: String,
    val nameIt: String,
    val sameAs: String,
    val isOrWasLanguageOf: String
) {
    companion object {
        fun german(isOrWasLanguageOf: String): RicoLanguage {
            return RicoLanguage(
                nameDe = "Deutsch",
                nameFr = "Allemand",
                nameIt = "Tedesco",
                sameAs = "https://www.wikidata.org/entity/Q188",
                isOrWasLanguageOf = isOrWasLanguageOf
            )
        }

        fun italian(isOrWasLanguageOf: String): RicoLanguage {
            return RicoLanguage(
                nameDe = "Italienisch",
                nameFr = "Italien",
                nameIt = "Italiano",
                sameAs = "https://www.wikidata.org/entity/Q652",
                isOrWasLanguageOf = isOrWasLanguageOf
            )
        }

        fun french(isOrWasLanguageOf: String): RicoLanguage {
            return RicoLanguage(
                nameDe = "Französisch",
                nameFr = "Français",
                nameIt = "Francese",
                sameAs = "https://www.wikidata.org/entity/Q150",
                isOrWasLanguageOf = isOrWasLanguageOf
            )
        }
    }
}

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoTitle(
    @EncodeDefault
    @SerialName("@type") val rdfType: RdfType = RdfType.TITLE,
    val type: TitleType,
    val titleDe: String?,
    val titleFr: String?,
    val titleIt: String?,
    val isOrWasTitleOf: String
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoSource(
    @EncodeDefault
    @SerialName("@type")
    val rdfType: RdfType = RdfType.RECORD_SET,
    val titleDe: String?,
    val titleFr: String?,
    val titleIt: String?,
    val sameAs: List<String>,
    val isSourceOf: String
)

@Serializable
data class SponsoringAgent(
    @SerialName("@id") val id: String? = null,
    @SerialName("@type") val rdfType: RdfType? = null,
    val type: SponsoringAgentType? = null,
    val titleDe: String? = null,
    val titleFr: String? = null,
    val titleIt: String? = null,
    val sameAs: List<String>? = null,
    val hasSponsoredResourceOfAgent: String? = null
) {
    companion object {
        fun memoriav(): SponsoringAgent {
            return SponsoringAgent(
                id = MB.memoriavInstitutionURI
            )
        }

        fun memoriavProject(
            titleDe: String?,
            titleFr: String?,
            titleIt: String?,
            sameAs: List<String>? = null,
            recordSet: String
        ): SponsoringAgent {
            return SponsoringAgent(
                rdfType = RdfType.RECORD_SET,
                type = SponsoringAgentType.MEMORIAV_PROJECT,
                titleDe = titleDe,
                titleFr = titleFr,
                titleIt = titleIt,
                sameAs = sameAs,
                hasSponsoredResourceOfAgent = recordSet,
            )
        }
    }
}

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoDate(
    @EncodeDefault @SerialName("@type") val rdfType: RdfType = RdfType.DATE_RANGE,
    val normalizedDateValue: String,
    val dateQualifierDe: String? = null,
    val dateQualifierFr: String? = null,
    val dateQualifierIt: String? = null,
    val isDateAssociatedWith: String,
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoSubjectRecord(
    @EncodeDefault @SerialName("@type") val rdfType: RdfType = RdfType.RECORD,
    val hasOrHadSubject: String,
    val sameAs: String?,
    val titleDe: String? = null,
    val titleFr: String? = null,
    val titleIt: String? = null,
    val type: RicoRecordResourceType = RicoRecordResourceType.PUBLICATION,
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoRecordResource(
    @EncodeDefault @SerialName("@type") val rdfType: RdfType,
    val isRecordResourceAssociatedWithRecordResource: String,
    val sameAs: String?,
    val titleDe: String? = null,
    val titleFr: String? = null,
    val titleIt: String? = null,
    val type: RicoRecordResourceType = RicoRecordResourceType.RELATED,
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class RicoRecordResourceHoldingRelation(
    @EncodeDefault @SerialName("@type") val rdfType: RdfType = RdfType.RECORD_RESOURCE_HOLDING_RELATION,
    val recordResourceHoldingRelationHasSource: String,
    val recordResourceHoldingRelationHasTarget: String,
    val type: RecordResourceHoldingRelationType,
)

@Serializable
enum class RecordResourceHoldingRelationType {
    @SerialName("original")
    ORIGINAL,

    @SerialName("master")
    MASTER,

    @SerialName("access")
    ACCESS
}

@Serializable
enum class RdfType {
    @SerialName("rico:RecordSet")
    RECORD_SET,

    @SerialName("rico:Identifier")
    IDENTIFIER,

    @SerialName("rico:Language")
    LANGUAGE,

    @SerialName("rico:Title")
    TITLE,

    @SerialName("rico:RecordResourceHoldingRelation")
    RECORD_RESOURCE_HOLDING_RELATION,

    @SerialName("rico:DateRange")
    DATE_RANGE,

    @SerialName("rico:Record")
    RECORD,

    @SerialName("rico:CorporateBody")
    INSTITUTION,

    @SerialName("rico:Place")
    PLACE,

    @SerialName("rico:Extent")
    EXTENT,

    @SerialName("rico:ContentType")
    CONTENT_TYPE,

    @SerialName("skos:Concept")
    SKOS_CONCEPT
}

@Serializable
enum class IdentifierType {
    @SerialName("main")
    MAIN,

    @SerialName("original")
    ORIGINAL,

    @SerialName("callNumber")
    CALL_NUMBER,

    @SerialName("oldMemobase")
    OLD_MEMOBASE,
}

@Serializable
enum class TitleType {
    @SerialName("main")
    MAIN,

    @SerialName("original")
    ORIGINAL
}

@Serializable
enum class RicoRecordResourceType {
    @SerialName("related")
    RELATED,
    @SerialName("publication")
    PUBLICATION,
}

@Serializable
enum class SponsoringAgentType {
    @SerialName("memoriavProject")
    MEMORIAV_PROJECT,
}