/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.institution

import ch.memobase.models.FacetContainer
import ch.memobase.models.data.LanguageCode
import ch.memobase.models.drupal.DrupalApiListResponse
import ch.memobase.models.drupal.DrupalApiResponse

data class InstitutionCompleteData(
    val institutionData: Map<LanguageCode, DrupalApiResponse>,
    val inventoryCollectionData: Map<LanguageCode, DrupalApiListResponse>,
    val recordSetsCount: Int,
    val totalDocumentCount: Long,
    val publishedDocumentCount: Long,
    val documentTypes: List<FacetContainer>,
)