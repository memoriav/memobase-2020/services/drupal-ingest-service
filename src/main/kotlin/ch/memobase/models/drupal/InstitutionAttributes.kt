/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.drupal

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class InstitutionAttributes(
    val status: Boolean,
    val title: String,
    @SerialName("field_memobase_id") val fieldMemobaseId: String,
    @SerialName("field_text") val fieldText: AttributeRichText?,
    @SerialName("field_survey_date") val fieldSurveyDate: String?,
    @SerialName("field_email") val fieldEmail: String?,
    @SerialName("field_isil") val fieldIsil: String?,
    @SerialName("field_memobase_institution") val fieldMemobaseInstitution: Boolean?,
    @SerialName("field_old_memobase_id") val fieldOldMemobaseId: String?,
    @SerialName("field_link_archive_catalog") val fieldLinkArchiveCatalog: AttributeLink?,
    @SerialName("field_private") val fieldPrivate: Boolean?,
    @SerialName("field_survey_institution") val fieldSurveyInstitution: Boolean?,
    @SerialName("field_teaser_color") val fieldTeaserColor: String?,
    @SerialName("field_website") val fieldWebsite: AttributeLink?,
    @SerialName("field_wikidata_id") val fieldWikidataId: AttributeLink?,
    @SerialName("field_accessibility") val fieldAccessibility: String?,
    @SerialName("computed_teaser_image_url") val computedTeaserImageUrl: String?,
    @SerialName("computed_teaser_color") val computedTeaserColor: String?,
)