/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.institution

import ch.memobase.models.recordset.RdfType
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoPlace @OptIn(ExperimentalSerializationApi::class) constructor(
    @EncodeDefault
    @SerialName("@type")
    val rdfType: RdfType = RdfType.PLACE,
    @SerialName("type")
    val ricoType: RicoPlaceTypeValue,
    val postalCode: String,
    val streetAddress: String,
    val streetNumber: String,
    val locatedOnStreet: String,
    val coordinateLocation: String,
    val country: String,
    val locatedInTheAdministrativeTerritorialEntity: List<AdministrativeTerritorialEntity>,
    val isOrWasLocationOf: String
)

