/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import ch.memobase.models.data.LanguageCode
import ch.memobase.models.drupal.AttributeLink
import ch.memobase.models.drupal.data.InstitutionData
import ch.memobase.models.drupal.data.LanguageOfMetadataData
import ch.memobase.models.drupal.data.RecordSetData
import ch.memobase.models.recordset.RecordSetCompleteData
import ch.memobase.utility.DrupalApiException
import ch.memobase.utility.InvalidDataException
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class RecordSetDataCollectionPipeline(
    private val drupalClient: DrupalApiClient,
    private val elasticClient: ElasticClient,
    private val documentsIndex: String,
) {
    companion object {
        /// This value is hard coded in drupal and needs to be matched.
        const val RELATED_RECORD_SET_MEMOBASE_ENTITY_VALUE: String = "entity:node"

        /// This value is hard coded in drupal and needs to be matched. This is the value when no link is available.
        const val RELATED_RECORD_SET_NO_LINK_VALUE: String = "route:<nolink>"
    }

    suspend fun collectRecordSetData(uuid: String): RecordSetCompleteData = coroutineScope {
        val deDeferred = async { drupalClient.getRecordSet(uuid, "de") }
        val frDeferred = async { drupalClient.getRecordSet(uuid, "fr") }
        val itDeferred = async { drupalClient.getRecordSet(uuid, "it") }

        val recordSetDe = deDeferred.await()
        val recordSetFr = frDeferred.await()
        val recordSetIt = itDeferred.await()


        val dataDe = recordSetDe.data as RecordSetData

        val documentIndexData = getDocumentIndexData(dataDe.attributes.fieldMemobaseId, documentsIndex)

        val metadataLanguages = mutableListOf<String>()
        val institutionMap = mutableMapOf<String, FacetContainer>()

        recordSetDe.included.map { data ->
            when (data) {
                is InstitutionData -> {
                    institutionMap.put(
                        data.id, FacetContainer(
                            name =
                            LanguageContainer().addValue(LanguageCode.DE, data.attributes.title),
                            filter = data.attributes.fieldMemobaseId
                        )
                    )
                }

                is LanguageOfMetadataData -> {
                    metadataLanguages.add(data.attributes.name)
                }


                else -> {
                    throw DrupalApiException("Unknown data type in included data: $data")
                }
            }
        }

        recordSetFr.included.map { data ->
            when (data) {
                is InstitutionData -> {
                    val institution = institutionMap[data.id]
                    if (institution != null) {
                        institutionMap[data.id] = institution.copy(
                            name = institution.name.addValue(LanguageCode.FR, data.attributes.title)
                        )
                    } else {
                        throw InvalidDataException("Institution not found in de data")
                    }
                }

                is LanguageOfMetadataData -> {
                    // is the same code as de, the translation is loaded from file.
                }

                else -> {
                    throw DrupalApiException("Unknown data type in included data: $data")
                }
            }
        }

        recordSetIt.included.map { data ->
            when (data) {
                is InstitutionData -> {
                    val institution = institutionMap[data.id]
                    if (institution != null) {
                        institutionMap[data.id] = institution.copy(
                            name = institution.name.addValue(LanguageCode.IT, data.attributes.title)
                        )
                    } else {
                        throw InvalidDataException("Institution not found in de data")
                    }
                }

                is LanguageOfMetadataData -> {
                    // is the same code as de, the translation is loaded from file.
                }

                else -> {
                    throw DrupalApiException("Unknown data type in included data: $data")
                }
            }
        }

        RecordSetCompleteData(
            dataDe,
            recordSetFr.data as RecordSetData,
            recordSetIt.data as RecordSetData,
            recordSetDe.data.relationships.fieldInstitution?.data?.map {
                institutionMap[it.id] ?: error("Institution not found")
            } ?: emptyList(),
            recordSetDe.data.relationships.fieldRespInstitutionAccess?.data?.map {
                institutionMap[it.id] ?: error("Access institution not found")
            } ?: emptyList(),
            recordSetDe.data.relationships.fieldRespInstitutionOriginal?.data?.map {
                institutionMap[it.id] ?: error("Original institution not found")
            } ?: emptyList(),
            recordSetDe.data.relationships.fieldRespInstitutionMaster?.data?.map {
                institutionMap[it.id] ?: error("Master institution not found")
            } ?: emptyList(),
            metadataLanguages,
            processRelatedRecordSets(
                dataDe.attributes.fieldRelatedRecordSets,
                recordSetFr.data.attributes.fieldRelatedRecordSets,
                recordSetIt.data.attributes.fieldRelatedRecordSets
            ),
            documentIndexData.first,
            documentIndexData.second,
            documentIndexData.third
        )
    }

    private fun processRelatedRecordSets(
        de: List<AttributeLink>,
        fr: List<AttributeLink>,
        it: List<AttributeLink>
    ): List<FacetContainer> {
        val mergedRelatedRecordSets = mutableMapOf<String, FacetContainer>()
        val unmergedRelatedRecordSets = mutableListOf<FacetContainer>()

        fun updateContainer(uri: String, title: String?, language: LanguageCode) {
            val container = mergedRelatedRecordSets[uri]
            if (container == null) {
                mergedRelatedRecordSets[uri] = FacetContainer(
                    name = LanguageContainer(
                        if (language == LanguageCode.DE) title else null,
                        if (language == LanguageCode.FR) title else null,
                        if (language == LanguageCode.IT) title else null
                    ),
                    filter = if (!uri.startsWith(RELATED_RECORD_SET_MEMOBASE_ENTITY_VALUE)) uri else null
                )
            } else {
                mergedRelatedRecordSets[uri] = FacetContainer(
                    name = if (title != null) container.name.addValue(language, title) else container.name,
                    filter = if (!uri.startsWith(RELATED_RECORD_SET_MEMOBASE_ENTITY_VALUE)) uri else null
                )
            }
        }

        fun processRecordSet(recordSet: AttributeLink, language: LanguageCode) {
            when {
                recordSet.uri.startsWith(RELATED_RECORD_SET_MEMOBASE_ENTITY_VALUE) -> {
                    updateContainer(recordSet.uri, recordSet.title, language)
                }
                recordSet.uri.startsWith(RELATED_RECORD_SET_NO_LINK_VALUE) -> {
                    unmergedRelatedRecordSets.add(FacetContainer(
                        name = LanguageContainer(
                            de = if (language == LanguageCode.DE) recordSet.title else null,
                            fr = if (language == LanguageCode.FR) recordSet.title else null,
                            it = if (language == LanguageCode.IT) recordSet.title else null
                        )
                    ))
                }
                else -> {
                    updateContainer(recordSet.uri, recordSet.title, language)
                }
            }
        }

        de.forEach { processRecordSet(it, LanguageCode.DE) }
        fr.forEach { processRecordSet(it, LanguageCode.FR) }
        it.forEach { processRecordSet(it, LanguageCode.IT) }

        return mergedRelatedRecordSets.values.toList() + unmergedRelatedRecordSets
    }

    private suspend fun getDocumentIndexData(
        memobaseId: String,
        documentsIndex: String,
    ): Triple<Int, Int, List<FacetContainer>> =
        coroutineScope {
            val totalNumberOfDocuments = async { elasticClient.countTotalNumberOfDocuments(memobaseId, documentsIndex) }
            val numberOfUnpublishedDocuments =
                async { elasticClient.countNumberOfDocumentsUnpublished(memobaseId, documentsIndex) }
            val documentTypes =
                async { elasticClient.getDocumentTypesFromRecords(memobaseId, documentsIndex) }

            val totalNumberOfDocumentsValue = totalNumberOfDocuments.await()
            val numberOfUnpublishedDocumentsValue = numberOfUnpublishedDocuments.await()
            Triple(
                totalNumberOfDocumentsValue.toInt(),
                (totalNumberOfDocumentsValue - numberOfUnpublishedDocumentsValue).toInt(),
                documentTypes.await()
            )
        }
}