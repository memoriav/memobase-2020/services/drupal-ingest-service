package ch.memobase

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import kotlin.test.assertEquals


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestConfiguration {

    @Test
    fun `test municipalities loader`() {
        val config = Configuration()
        assertAll(
            {
                assertEquals(config.application.host, "https://localhost:8080")
            }
        )
    }
}