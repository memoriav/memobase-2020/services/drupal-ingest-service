/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.institution

import ch.memobase.models.recordset.RdfType
import ch.memobase.models.recordset.RicoIdentifier
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import java.io.Reader
import java.io.StringReader

@Serializable
data class InstitutionJsonLdDocument @OptIn(ExperimentalSerializationApi::class) constructor(
    @EncodeDefault
    @SerialName("@context")
    val context: String = "https://api.memobase.ch/contexts/institution.json",
    @SerialName("@id")
    val id: String,
    @EncodeDefault
    @SerialName("@type")
    val rdfType: RdfType = RdfType.INSTITUTION,
    @SerialName("type")
    val ricoType: InstitutionTypeValue,
    val nameDe: String,
    val nameFr: String,
    val nameIt: String,
    val descriptiveNoteDe: String?,
    val descriptiveNoteFr: String?,
    val descriptiveNoteIt: String?,
    val isPublished: Boolean,
    @SerialName("ISIL")
    val isil: String?,
    val instanceOf: List<String>,
    val image: String?,
    val emailAddress: String?,
    val officialWebsite: String?,
    val url: String?,
    val sameAs: String?,
    val hasOrHadIdentifier: RicoIdentifier,
    val hasOrHadLocation: List<RicoPlace>,
    val isOrWasHolderOf: MemobaseSurveyRecordSetParent,
) {
    @OptIn(ExperimentalSerializationApi::class)
    fun getReader(): Reader {
        val json = Json {
            explicitNulls = false
        }
        return StringReader(json.encodeToString(this))
    }
}

