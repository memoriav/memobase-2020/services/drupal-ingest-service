/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.models.config.DrupalApiClientConfiguration
import ch.memobase.models.drupal.DrupalApiListResponse
import ch.memobase.models.drupal.DrupalApiResponse
import ch.memobase.models.drupal.DrupalResponseSimple
import ch.memobase.models.drupal.data.InstitutionInventarData
import ch.memobase.models.drupal.data.RecordSetData
import ch.memobase.utility.DrupalApiException
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.delay
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.LogManager
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.math.pow


class DrupalApiClient(cfg: DrupalApiClientConfiguration) : Closeable {

    companion object {
        const val JSON_API_HEADER = "X-API-Key"
        const val JSONAPI_PATH_SEG = "jsonapi"
        const val JSONAPI_PATH_NODE = "node"
        const val JSONAPI_PATH_RECORD_SET = "record_set"
        const val JSONAPI_PATH_INSTITUTION = "institution"
        const val JSONAPI_PATH_INSTITUTION_INVENTORY_DATA = "institution_inventar_data"

        const val PARAM_NAME_INCLUDE = "include"
        const val PARAM_VALUE_INCLUDE_INSTITUTION =
            "field_institution_types,field_other_institution_types,field_extended_address"
        const val PARAM_VALUE_INCLUDE_INSTITUTION_INVENTORY_DATA =
            "field_document_type,field_time,field_geo,field_subjects"
        const val PARAM_VALUE_INCLUDE_RECORD_SET =
            "field_institution,field_metadata_languages,field_resp_institution_access,field_resp_institution_master,field_resp_institution_original"
        const val PARAM_NAME_FILTER_FILTER_INSTITUTION_ID = "filter[field_institution.id]"
    }

    private val apiKey = cfg.apiKey
    private val user = cfg.user
    private val password = cfg.password
    private val domain = cfg.domain
    private val scheme = URLProtocol.HTTPS

    private val log = LogManager.getLogger(this::class.java)
    private val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(json = Json {
                ignoreUnknownKeys = true
            })
        }
    }


    @OptIn(ExperimentalEncodingApi::class)
    private val authorizationCredentials = if (user.isNotBlank() && password.isNotBlank())
        Base64.encode("$user:$password".toByteArray(Charsets.UTF_8)) else ""

    suspend fun getAllInstitutionUuids(): List<String> {
        val url = URLBuilder(
            protocol = URLProtocol.HTTPS,
            host = domain,
            pathSegments = listOf(
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_INSTITUTION
            )
        ).build()
        log.info("Fetching all institution uuids from $url")
        var response = request(url, 1)

        val allIds = mutableListOf<String>()
        return when (response.status) {
            HttpStatusCode.OK -> {
                var drupalResponse = try {
                    response.body<DrupalResponseSimple>()
                } catch (e: JsonConvertException) {
                    throw DrupalApiException("Could not parse institution uuids response: ${e.message}. Message: ${response.body<String>()}")
                }
                var next = drupalResponse.links.next
                allIds.addAll(drupalResponse.data.map { it.id })
                while (next != null) {
                    val nextUrl = next.href
                    log.info("Fetching next page of institution uuids from $nextUrl")
                    response = client.get(nextUrl) {
                        setupRequestHeaders(this)
                    }
                    drupalResponse = try {
                        response.body<DrupalResponseSimple>()
                    } catch (e: JsonConvertException) {
                        throw DrupalApiException("Could not parse institution uuids response: ${e.message}. Message: ${response.body<String>()}")
                    }
                    next = drupalResponse.links.next
                    allIds.addAll(drupalResponse.data.map { it.id })
                }
                allIds
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch institution uuids. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getAllRecordSetUuids(): List<String> {
        val url = URLBuilder(
            protocol = URLProtocol.HTTPS,
            host = domain,
            pathSegments = listOf(
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_RECORD_SET
            )
        ).build()
        log.info("Fetching all record set uuids from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                var drupalResponse = try {
                    response.body<DrupalResponseSimple>()
                } catch (e: JsonConvertException) {
                    throw DrupalApiException("Could not parse record set uuids response: ${e.message}. Message: ${response.body<String>()}")
                }
                val allIds = mutableListOf<String>()
                var next = drupalResponse.links.next
                allIds.addAll(drupalResponse.data.map { it.id })
                while (next != null) {
                    val nextUrl = next.href
                    log.info("Fetching next page of record set uuids from $nextUrl")
                    val nextResponse = client.get(nextUrl) {
                        setupRequestHeaders(this)
                    }
                    drupalResponse = try {
                        nextResponse.body<DrupalResponseSimple>()
                    } catch (e: JsonConvertException) {
                        throw DrupalApiException("Could not parse record set uuids response: ${e.message}. Message: ${nextResponse.body<String>()}")
                    }
                    next = drupalResponse.links.next
                    allIds.addAll(drupalResponse.data.map { it.id })
                }
                allIds
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch record set uuids. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getRecordSet(recordSetNodeId: String, language: String): DrupalApiResponse {
        val url = URLBuilder(
            protocol = URLProtocol.HTTPS,
            host = domain,
            pathSegments = listOf(
                language,
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_RECORD_SET,
                recordSetNodeId
            ),
            parameters = ParametersBuilder().apply { append(PARAM_NAME_INCLUDE, PARAM_VALUE_INCLUDE_RECORD_SET) }
                .build()
        ).build()
        log.info("Fetching record set $recordSetNodeId from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                try {
                    response.body<DrupalApiResponse>()
                } catch (e: JsonConvertException) {
                    throw DrupalApiException("Could not parse record set $recordSetNodeId response: ${e.message}. Message: ${response.body<String>()}")
                }
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            HttpStatusCode.NotFound -> {
                throw DrupalApiException("Not Found. The requested record set id $recordSetNodeId does not exist.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch record set $recordSetNodeId. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getRecordSetIdsWithFilter(institutionUuid: String): List<String> {
        val url = URLBuilder(
            protocol = URLProtocol.HTTPS,
            host = domain,
            pathSegments = listOf(
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_RECORD_SET
            ),
            parameters = ParametersBuilder().apply {
                append(PARAM_NAME_FILTER_FILTER_INSTITUTION_ID, institutionUuid)
            }.build()
        ).build()
        log.info("Fetching record set for institution $institutionUuid from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                val drupalResponse = try {
                    response.body<DrupalApiListResponse>()
                } catch (e: JsonConvertException) {
                    log.error(e.message, e)
                    log.error("Could not parse record set $institutionUuid response. Message: ${response.body<String>()}")
                    null
                }
                drupalResponse?.data?.filterIsInstance<RecordSetData>()?.map { it.attributes.fieldMemobaseId }
                    ?: emptyList()
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            HttpStatusCode.NotFound -> {
                throw DrupalApiException("Not Found. The requested institution id $institutionUuid does not exist.")
            }

            else -> {
                throw DrupalApiException("Could not fetch record sets for institution $institutionUuid. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getInstitution(institutionNodeId: String, language: String): DrupalApiResponse {
        val url = URLBuilder(
            protocol = scheme,
            host = domain,
            pathSegments = listOf(
                language,
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_INSTITUTION,
                institutionNodeId
            ),
            parameters = ParametersBuilder().apply { append(PARAM_NAME_INCLUDE, PARAM_VALUE_INCLUDE_INSTITUTION) }
                .build()
        ).build()
        log.info("Fetching institution $institutionNodeId from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                try {
                    response.body<DrupalApiResponse>()
                } catch (e: JsonConvertException) {
                    log.error(e.message, e)
                    throw DrupalApiException("Could not parse institution $institutionNodeId response.")
                }
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            HttpStatusCode.NotFound -> {
                throw DrupalApiException("Not Found. The requested institution id $institutionNodeId does not exist.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch institution $institutionNodeId. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getInventoryDataCollectionInstitutionId(inventoryCollectionUuid: String): String {
        val url = URLBuilder(
            protocol = scheme,
            host = domain,
            pathSegments = listOf(
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_INSTITUTION_INVENTORY_DATA,
                inventoryCollectionUuid
            )
        ).build()
        log.info("Fetching institution id for inventory collection $inventoryCollectionUuid from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                val responseBody = try {
                    response.body<DrupalApiResponse>()
                } catch (e: JsonConvertException) {
                    log.error(e.message, e)
                    throw DrupalApiException("Could not parse institution id for inventory collection $inventoryCollectionUuid response.")
                }
                val data = responseBody.data as InstitutionInventarData
                data.relationships.fieldInstitution?.data?.firstOrNull()?.id
                    ?: throw DrupalApiException("Could not find institution id for inventory collection $inventoryCollectionUuid.")
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch institution id for inventory collection $inventoryCollectionUuid. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }

    suspend fun getInventoryDataCollections(institutionUuid: String, language: String): DrupalApiListResponse {
        val url = URLBuilder(
            protocol = scheme,
            host = domain,
            pathSegments = listOf(
                language,
                JSONAPI_PATH_SEG,
                JSONAPI_PATH_NODE,
                JSONAPI_PATH_INSTITUTION_INVENTORY_DATA
            ),
            parameters = ParametersBuilder().apply {
                append(PARAM_NAME_FILTER_FILTER_INSTITUTION_ID, institutionUuid)
                append(PARAM_NAME_INCLUDE, PARAM_VALUE_INCLUDE_INSTITUTION_INVENTORY_DATA)
            }.build()
        ).build()
        log.info("Fetching inventory data for institution $institutionUuid from $url")
        val response = request(url, 1)

        return when (response.status) {
            HttpStatusCode.OK -> {
                try {
                    response.body<DrupalApiListResponse>()
                } catch (e: JsonConvertException) {
                    log.error(e.message, e)
                    log.error("Could not parse inventory data for institution $institutionUuid response. Message: ${response.body<String>()}")
                    throw DrupalApiException("Could not parse inventory data for institution $institutionUuid response.")
                }
            }

            HttpStatusCode.Unauthorized -> {
                throw DrupalApiException("Unauthorized access.")
            }

            else -> {
                throw DrupalApiException("Failed to fetch inventory data for institution $institutionUuid. Status: ${response.status}. Message: ${response.body<String>()}")
            }
        }
    }


    private fun setupRequestHeaders(builder: HttpRequestBuilder) {
        builder.apply {
            headers {
                append(JSON_API_HEADER, apiKey)
                if (authorizationCredentials.isNotBlank()) {
                    append(HttpHeaders.Authorization, "Basic $authorizationCredentials")
                }
                append(HttpHeaders.Accept, ContentType.Application.Json.toString())
                append(HttpHeaders.UserAgent, "Drupal Ingest Service")
            }
        }
    }

    private suspend fun request(url: Url, attempt: Int): HttpResponse {
        return try {
            client.get(url) {
                setupRequestHeaders(this)
            }
        } catch (ex: HttpRequestTimeoutException) {
            if (attempt < 5) {
                val delayDuration = (1000 * 2.0.pow(attempt.toDouble())).toLong()
                log.warn("Request to $url timed out. Retrying attempt $attempt after delay of $delayDuration ms")
                delay(delayDuration)
                request(url, attempt + 1)
            } else {
                throw DrupalApiException("Connection to Drupal timed out. Try importing again.")
            }
        }
    }

    override fun close() {
        client.close()
    }
}
