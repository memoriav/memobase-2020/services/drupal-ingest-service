/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.plugins

import ch.memobase.InstitutionIngestPipeline
import ch.memobase.RecordSetIngestPipeline
import ch.memobase.configuration
import ch.memobase.impl.DrupalApiClient
import ch.memobase.impl.ElasticClient
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.launch

fun Application.configureRouting() {
    val elasticClient = ElasticClient(configuration.elasticSearchConfig)
    val drupalClient = DrupalApiClient(configuration.drupal)
    val recordSetIngestPipeline = RecordSetIngestPipeline(
        drupalClient,
        elasticClient,
        configuration.indexConfig,
    )
    val institutionIngestPipeline = InstitutionIngestPipeline(
        drupalClient,
        elasticClient,
        configuration.indexConfig,
        configuration.cantons,
        configuration.municipalities,
        configuration.inventoryDocumentTypes,
        configuration.inventoryGeo,
        configuration.inventoryTimePeriod,
    )

    routing {
        put("/api/v1/recordset/{uuid}/{memobaseId}") {
            val uuid = call.parameters["uuid"]
            if (uuid == null) {
                call.application.log.error("UUID is required")
                call.respond(HttpStatusCode.BadRequest, "UUID is required")
                return@put
            }
            val memobaseId = call.parameters["memobaseId"]
            if (memobaseId == null) {
                call.application.log.error("Memobase ID is required")
                call.respond(HttpStatusCode.BadRequest, "Memobase ID is required")
                return@put
            }
            call.application.log.info("Ingesting record set with UUID $uuid")
            call.respond(HttpStatusCode.OK, "Ingest started for UUID $uuid")
            launch {
                recordSetIngestPipeline.ingest(uuid, memobaseId)
            }
        }

        put("/api/v1/recordset/{uuid}") {
            val uuid = call.parameters["uuid"]
            if (uuid == null) {
                call.application.log.error("UUID is required")
                call.respond(HttpStatusCode.BadRequest, "UUID is required")
                return@put
            }
            call.application.log.info("Ingesting record set with UUID $uuid")
            call.respond(HttpStatusCode.OK, "Ingest started for UUID $uuid")
            launch {
                recordSetIngestPipeline.ingest(uuid, "unknown")
            }
        }

        put("/api/v1/institution/{uuid}") {
            val uuid = call.parameters["uuid"]
            if (uuid == null) {
                call.application.log.error("UUID is required")
                call.respond(HttpStatusCode.BadRequest, "UUID is required")
                return@put
            }
            call.application.log.info("Ingesting institution with UUID $uuid.")
            call.respond(HttpStatusCode.OK, "Ingest started for UUID $uuid.")
            launch {
                institutionIngestPipeline.ingest(uuid, "unknown")
            }
        }

        put("/api/v1/institution/{uuid}/{memobaseId}") {
            val uuid = call.parameters["uuid"]
            if (uuid == null) {
                call.application.log.error("UUID is required")
                call.respond(HttpStatusCode.BadRequest, "UUID is required")
                return@put
            }
            val memobaseId = call.parameters["memobaseId"]
            if (memobaseId == null) {
                call.application.log.error("Memobase ID is required")
                call.respond(HttpStatusCode.BadRequest, "Memobase ID is required")
                return@put
            }
            call.application.log.info("Ingesting institution with UUID $uuid and Memobase ID $memobaseId")
            call.respond(HttpStatusCode.OK, "Ingest started for UUID $uuid and Memobase ID $memobaseId")
            launch {
                institutionIngestPipeline.ingest(uuid, memobaseId)
            }
        }

        put("/api/v1/inventorycollection/{uuid}") {
            val uuid = call.parameters["uuid"]
            if (uuid == null) {
                call.application.log.error("UUID is required")
                call.respond(HttpStatusCode.BadRequest, "UUID is required")
                return@put
            }
            call.application.log.info("Ingesting inventory collection with UUID $uuid")
            call.respond(HttpStatusCode.OK, "Ingest started for UUID $uuid")
            launch {
                institutionIngestPipeline.ingestInventoryCollection(uuid)
            }
        }

        put("api/v1/institution/all") {
            call.application.log.info("Ingesting all institutions")
            call.respond(HttpStatusCode.OK, "Ingest started for all institutions")
            launch {
                institutionIngestPipeline.ingestAll()
            }
        }

        put("/api/v1/recordset/all") {
            call.application.log.info("Ingesting all record sets")
            call.respond(HttpStatusCode.OK, "Ingest started for all record sets")
            launch {
                recordSetIngestPipeline.ingestAll()
            }
        }
    }
}

