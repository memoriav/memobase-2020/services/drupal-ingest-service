/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.models.institution

import ch.memobase.models.FacetContainer
import ch.memobase.models.LanguageContainer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.Reader
import java.io.StringReader

@Serializable
data class InstitutionDocument(
    val id: String,
    val drupalUuid: String,
    val published: Boolean,
    val fullInstitutionPage: Boolean,
    val surveyInstitution: Boolean,
    val private: Boolean,

    // Full Text Search
    val name: LanguageContainer,
    val description: LanguageContainer,

    // Facet Fields
    val type: List<FacetContainer>,
    val documentType: List<FacetContainer>,

    val mainAddress: InstitutionAddress,
    val additionalAddresses: List<InstitutionAddress>,

    // Sort
    val lastUpdatedDate: String,

    // Search Result Display
    val keyVisualLink: String?,
    val numberOfRecordSets: Int,
    val numberOfDocuments: Int,
    val numberOfDocumentsPublished: Int,
    val teaserColor: String?,
    val teaserColorComputed: String?,

    val combined: InventoryCollection?,
    val film: InventoryCollection?,
    val image: InventoryCollection?,
    val video: InventoryCollection?,
    val sound: InventoryCollection?,
    val other: InventoryCollection?
) {
    @OptIn(ExperimentalSerializationApi::class)
    fun getReader(): Reader {
        val json = Json {
            explicitNulls = false
        }
        return StringReader(json.encodeToString(this))
    }
}